//������ ������� �������� ���������� �����
//������������ �������
//��� ������ (��������������� / ��������������)
//������� ������ 1:500/1:1000/1:2000/1:5000
//������������� (��������� - ���)

tg_plan_parameters : dialog { label="��������� ���������� �����"; width=70;

	:edit_box 	{label="������������ �������:"; key="plan_name";}
	:edit_box 	{label="������������ �����������:"; key="organization";}
	:popup_list	{label="��� ������:"; key="plan_type";
			 list="��������������� \n��������������";
			 fixed_width_font=true;}
	:popup_list	{label="������� ������:"; key="plan_scale";
			 list="1:500 \n1000 \n2000 \n5000";
			 fixed_width_font=true;}
	:spacer		{height=1;}
	:column		{label="�������������"; key="�������";

			:row	{key="���1";
				:edit_box {label="���������:"; key="percon_job0";}
				:edit_box {label="�������:"; key="percon_name0";}
				}
			:row	{key="���2";
				:edit_box {label="���������:"; key="percon_job1";}
				:edit_box {label="�������:"; key="percon_name1";}
				}
			:row	{key="���3";
				:edit_box {label="���������:"; key="percon_job2";}
				:edit_box {label="�������:"; key="percon_name2";}
				}
			:row	{key="���4";
				:edit_box {label="���������:"; key="percon_job3";}
				:edit_box {label="�������:"; key="percon_name3";}
				}
			:row	{key="���5";
				:edit_box {label="���������:"; key="percon_job4";}
				:edit_box {label="�������:"; key="percon_name4";}
				}
			:row	{key="���6";
				:edit_box {label="���������:"; key="percon_job5";}
				:edit_box {label="�������:"; key="percon_name5";}
				}
			:spacer	{height=1;}
			}
	:spacer		{height=1;}
	ok_cancel;

}//����� �������



tg_default_parameters : dialog { label="��������� ��-���������"; width=100;

	:edit_box 	{label="������������ �����������:"; key="organization";}
	:spacer		{height=1;}
	:column		{label="�������������"; key="�������";

			:row	{key="���1";
				:edit_box {label="���������:"; key="percon_job0";}
				:edit_box {label="�������:"; key="percon_name0";}
				}
			:row	{key="���2";
				:edit_box {label="���������:"; key="percon_job1";}
				:edit_box {label="�������:"; key="percon_name1";}
				}
			:row	{key="���3";
				:edit_box {label="���������:"; key="percon_job2";}
				:edit_box {label="�������:"; key="percon_name2";}
				}
			:row	{key="���4";
				:edit_box {label="���������:"; key="percon_job3";}
				:edit_box {label="�������:"; key="percon_name3";}
				}
			:row	{key="���5";
				:edit_box {label="���������:"; key="percon_job4";}
				:edit_box {label="�������:"; key="percon_name4";}
				}
			:row	{key="���6";
				:edit_box {label="���������:"; key="percon_job5";}
				:edit_box {label="�������:"; key="percon_name5";}
				}
			:spacer	{height=1;}
			}
	:spacer		{height=1;}
	:edit_box	{label="���� ������-����������:"; key="path";}
	:row		{key=r1;
			:spacer	{width=1;}
			:button	{key="search_bt"; label="�����"; width=8; fixed_width=true;}
			:spacer	{width=1;}
			:spacer	{width=1;}
			}
	
	:spacer		{height=1;}
	:edit_box	{label="���� ���������� ������:"; key="block_lib";}
	:row		{key=r2;
			:spacer	{width=1;}
			:button	{key="search_bl"; label="�����"; width=8; fixed_width=true;}
			:spacer	{width=1;}
			:spacer	{width=1;}
			}
	ok_cancel;

}//����� �������



tg_compliment_krd	:dialog {label="���������� ����� ���������";
				 width=50;
				 :spacer		{height=1;}
				 :column {label="������� ������:";
					 :spacer		{height=1;}
					 :edit_box {label="    krd ����:"; key="krd_file";}
					 :row		{key=r1;
						:spacer	{width=1;}
						:button	{key="search_krd"; label="�����"; width=8; fixed_width=true;}
						:spacer	{width=1;}
						:spacer	{width=1;}
						}

					 :edit_box {label="     sdr ����:"; key="sdr_file";}
					 :row		{key=r2;
						:spacer	{width=1;}
						:button	{key="search_sdr"; label="�����"; width=8; fixed_width=true;}
						:spacer	{width=1;}
						:spacer	{width=1;}
						}
					:edit_box {label="           ����:"; key="date";}
					:spacer		{height=1;}
				}
					:spacer		{height=2;}
				:column {label="������� ������:";
					:edit_box {label="  tgkrd ����:"; key="tgkrd_file";}
					:row		{key=r3;
						:spacer	{width=1;}
						:button	{key="search_tgkrd"; label="�����"; width=8; fixed_width=true;}
						:spacer	{width=1;}
						:spacer	{width=1;}
						}
					:spacer		{height=1;}
				}
				:spacer		{height=1;}
				ok_cancel;



				}//end dialog


tg_import_krd_file	:dialog {label="������ ������� �� ������������� �����"; width=120;

				:spacer		{height=1;}
				:row 	{
					:column {
						:popup_list {label="��:"; key="SK";}
						:popup_list {label="��� �� :"; key="SK_type";}
						:popup_list {label="����� ������:"; key="keyconfig";}
						:spacer	    {height=1;}
						:edit_box   {label="������� �������:"; key="onimport";}
						:spacer	    {height=7;}
						}
					
					:column {
						:row 	{
							:column {width=5;
								:spacer		{height=1;}
								:button {label="+"; key="plus";  width=2; fixed_width=true; alignment=right; is_bold=true;}
								:button {label="-"; key="minus"; width=2; fixed_width=true; alignment=right; is_bold=true;}
								:button {label="X"; key="clear"; width=2; fixed_width=true; alignment=right; is_bold=true;}
								:spacer {height=7;}								
								}

							:list_box { key="file_list"; height=15; label="�����"; width=70; multiple_select=false;
								
								}
							}
						}
					}

				:list_box {label="������������"; key="preview"; height=30;}
				:spacer		{height=2;}
				ok_cancel;
}


tg_search_points	:dialog { label="������ �������"; width=100;

				:spacer		{height=1;}
				:row	{
					:column { label="������� �������";
						:spacer		{height=1;}
						:list_box { key="onimport_list"; height=20; width=12; multiple_select=true;}
						:row 	{
							:button {label="���"; key="onimport_all";  width=2; fixed_width=true; alignment=right; is_bold=true;}
							:button {label="��������"; key="onimport_clear"; width=2; fixed_width=true; alignment=right; is_bold=true;}
							}
						:spacer		{height=1;}
						}//column
					:column { label="�����";
						:spacer		{height=1;}
						:list_box { key="blocks_list"; height=20; width=12; multiple_select=true;}
						:row 	{
							:button {label="���"; key="blocks_all";  width=2; fixed_width=true; alignment=right; is_bold=true;}
							:button {label="��������"; key="blocks_clear"; width=2; fixed_width=true; alignment=right; is_bold=true;}
							}
						:spacer		{height=1;}
						}//column
					:column { label="����";
						:spacer		{height=1;}
						:list_box { key="kodes_list"; height=20; width=12; multiple_select=true;}
						:button {label="��������"; key="kodes_clear"; width=2; fixed_width=true; alignment=right; is_bold=true;}
						:toggle {label="���221"; key="kodes_all";  width=2; fixed_width=true; alignment=right; is_bold=true;}
						:spacer		{height=1;}
						}//column
							}//row
					
				:spacer		{height=1;}
				:radio_column 	{key="method";
							:radio_button {key="select"; label="������� �� �����"; value="1";}
							:radio_button {key="to_file"; label="������� ������ � ����"; value="0";}
						}
				
				:spacer	{width=1;}
				:popup_list {label="                                   ��:"; key="SK";}
				:edit_box {label="�������������� ����:"; key="res_file";}
					:row		{
						:spacer	{width=1;}
						:button	{key="search_res"; label="�����"; width=8; fixed_width=true;}
						:spacer	{width=1;}
						:spacer	{width=1;}
						}
				:spacer		{height=1;}	


				ok_cancel;
}



tg_import_points_from_pline	:dialog { label="������� ����� �� ���������"; width=70;

					:spacer	{height=1;}
					:row 	{
						:spacer	{width=1;}
						:button	{key="search_obj"; label="������� �������"; width=10; fixed_width=true;}
						:spacer	{width=1;}
						}
					:spacer	{height=2;}
					:edit_box {label="                 ��� �����:"; key="kod";}
					:edit_box {label="������� �������:"; key="onimport";}
					:spacer	{height=2;}
					ok_cancel;
}


tg_refresh_points		:dialog {label="������� ����� � ������� �������"; width=80;

					:spacer	{height=1;}
					:row	{
						:column	{
							:spacer	{height=2;}
							:list_box { key="kodes_list"; height=20; width=25; multiple_select=true;}
							}
						:column	{
							:spacer	{height=4;}
							:button {label="->"; key="move_to"; fixed_width=true; alignment=right; is_bold=true;}
							:button {label=">>>"; key="move_all_to"; fixed_width=true; alignment=right; is_bold=true;}
							:button {label="<-"; key="move_out"; fixed_width=true; alignment=right; is_bold=true;}
							:button {label="<<<"; key="move_all_out"; fixed_width=true; alignment=right; is_bold=true;}
							:spacer	{height=4;}
							}
						:column	{
							:edit_box { key="onimport"; label="������� �������:";}
							:list_box { key="kodes_sel_list"; height=20; width=12; multiple_select=true;}
							}
						}
					:spacer {height=2;}
					ok_cancel;
					}
tg_attrib_change_dialog		:dialog	{label="��������� ��������� ����������"; width=40;
					:list_box { key="atributes_list"; height=20; width=30; multiple_select=true;}
					ok_cancel;
}

tg_one_attr_change_dialog	:dialog	{label="����� ��������"; width=100;
					:edit_box {label="����� ��������"; key="attr";}
					ok_cancel;
}


