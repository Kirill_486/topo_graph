(defun tg_read_file_information (inp_file / str res)
;;;
;;;��������� ������ ������ �� ����� � ��������� ��
;;;� ������ �� �������
;;;
(setq inp_file (open inp_file "r"))
(while (/= (setq str (read-line inp_file)) nil)

  (setq res (append res (list (TG_str_to_list_num str))))  

);end while
(close inp_file)
res
);end defun


(defun tg_read_file_list_strings (inp_file / str res)
;;;
;;;��������� ������ ������ �� ����� � ��������� ��
;;;� ������ �� �������
;;;
(setq inp_file (open inp_file "r"))
(while (/= (setq str (read-line inp_file)) nil)

  (setq res (append res (list (TG_str_to_list str))))  

);end while
(close inp_file)
res
);end defun



(defun tg_read_file_strings (inp_file / str res)
;;;
;;;��������� ������ ������ �� ����� 
;;;� ������ �� � ���� ������ �����
;;;
(setq inp_file (open inp_file "r"))
(while (/= (setq str (read-line inp_file)) nil)
  (setq res (append res (list str)))  
);end while
(close inp_file)
res
);end defun


(defun tg_save_list_to_file (out_file out_list /)

;;;��������� ������ � ����
;;;����� ��� ����� ��������� ���������
;;;  tg_read_file_information  � tg_read_file_strings
(setq out_file (open out_file "w"))
(mapcar (function (lambda (x / str)

				(if (= (type x) 'list) (progn 	(setq str "")
						       		(mapcar (function
								 (lambda (z)
									(setq str (strcat str " " (vl-princ-to-string z)))
								   )
								 )x)) (setq str (vl-princ-to-string x)) )
		    (write-line str out_file)
		    )) out_list)  

(close out_file)

  );end defun


(defun tg_load_block_library (block_list / need_import block_lib)

;;;������� ���������� � ������ ������� ���������� ������
;;;��������� ��� �� ����� �� ������ block_list ���� � �������
;;;����� ������� ����������.
;;;������ ��������� �� ������ ���� ����� � ���������� ���
  
(mapcar (function (lambda (x)
		    (if (not (tblsearch "block" x))
		      (setq need_import (setq need_import t))
		    ));end lambda
		  ) block_list )  

(if (and need_import (setq block_lib (tg_read_Path_block_lib_from_reg)) (findfile block_lib))
(progn (vl-cmdf "_.insert" block_lib) (vl-cmdf) (setq need_import nil))

(progn () );else

);end if

(mapcar (function (lambda (x)
		    (if (not (tblsearch "block" x))
		      (setq need_import (append need_import (list x)))
		    ));end lambda
		  ) block_list )  
(if need_import (progn (alert (strcat "� ���������� ������ ����������� �������� ������:" (tg_list_to_string need_import)))(exit)))
  
);end defun



(defun tg_read_sdr_information (inp_file / str sdr)

(vl-remove-if 'null(mapcar (function (lambda (x / res)

		    (if (= (substr x 1 4) "02TP")
		      (setq res (TG_str_to_list (strcat (substr x 5 16) " " (substr x 85 16)))))

		    (if (or (= (substr x 1 4) "09F1") (= (substr x 1 4) "09F2"))
		      (setq res (TG_str_to_list (strcat (substr x 25 12) " " (substr x 85 16)))))

		    res

		    )
		  ) (tg_read_file_strings inp_file)))
  

);end defun

(defun tg_read_key_from_file (target_key / keys key l_len res key_val inp_file)
;;;��������� ������ ������������ ������-���������� �� ����� inp_file
;;;((("[KEYCONFIG]" "MAIN") ("GD" "5_UKZ") ("*_" "5_NRL")) (("[KEYCONFIG]" "SIMPLE")....
;;;  

(setq inp_file (tg_read_Path_keyconfig_from_reg))
(setq keys (vl-remove-if 'null (mapcar (function (lambda (x) (if (and (/= x nil)(TG_member_match "\\*" (list (nth 0 x)))) nil x ) ))
		    (tg_read_file_list_strings inp_file))))
(mapcar (function (lambda (x) (if (= (car x ) "[KEYCONFIG]") (setq key (append key (list (vl-position x keys)) )) ))) keys)
(setq l_len (length keys) i 0)
(while (< i l_len)
(setq key_val (append key_val (list (nth i keys))))  
(if (or (and (/= i 0) (member (1+ i) key)) (= i (1- l_len)))
(setq res (append res (list key_val)) key_val nil)  
);end if
  (setq i (1+ i))  
);end while
  (if target_key (mapcar (function (lambda (x) (if (= (cadr (assoc "[KEYCONFIG]" x)) target_key ) (setq key_val (cdr x))))) res) (setq key_val res))
  key_val
);end defun

(defun tg_compliment_krd_with_field_data (krd_data kod_data date / )
;;; ��������� ��������� ������� ���������
;;; ����������� � �����
;;;����� ��� ������ � ����� ���
;;;���������� � krd ����
;;;���� � ���� ���������
;;;krd_data-������ ��� ����� (����� X Y Z)
;;;kod_data-������ � ����    (����� ���)      
;;;date-���� ���������	     ������ ��� ��������
(mapcar (function
	  (lambda (x)
		(append x (list (cadr (assoc (car x) kod_data))) (list date))
	    ))
	    krd_data )
);end defun


(defun tg_key_match (key keyconfig / res)
;������� ��� ����� ��� ���� �� ���������� ���������
;(("GD" "5_UKZ") ("*_" "5_NRL") ("VHOD" "5_UKZ") ("H_D" "5_NRL")......)
;
(if (not (null key)) (cadar (vl-remove-if-not (function (lambda (x) (wcmatch key (car x)))) keyconfig))

(cadr (last keyconfig));else
  );end if
);end defun


(defun tg_translate_to_WorldCS (inp_data /)

;;;������ X � Y ��� ������� ��
;;;��������� Z �� -1
;;;
;;;
(mapcar (function (lambda (x) (append (list (car x) (caddr x) (cadr x) (* -1 (cadddr x))) (cddddr x) )))inp_data)
       
);end defun

(defun tg_import_points_from_file (inp_file ucs keys onimport_name cs_type / scale entrys x-blocks x-kodes kkk old_ucs params snap)

;;;������� ���������� ����� �� ����� inp_file (���������) ��� ��������� ����� � �� ucs (���)
;;;��� �������������� ���������� ����� ������-���������� keys (���) � ������� �������
;;;������� onimport_name (��� ���� ���, �� �� �������)
;;;��������� ���������� � ����� ������ � 	"TG_onimport_entries" "TG_onimport_blocks"
;;;���������� � ����� ����� � 			"TG_onimport_entries" "TG_onimport_kodes"
;;;���� CS_type=0 - �� �������������
;;;���� CS_type=1 - �� �������

(setq inp_file (tg_read_file_information inp_file))

(if (= CS_type 1) (setq inp_file (tg_translate_to_WorldCS inp_file)))
(setq keys (tg_read_key_from_file keys))

(mapcar (function (lambda (x) (setq kkk (append kkk (list (cadr x)))))) keys)
(tg_load_block_library kkk)

(setq x-blocks (mapcar 'cdr (tg_x-record_load "TG_onimport_entries" "TG_onimport_blocks")))
(setq x-kodes  (mapcar 'cdr (tg_x-record_load "TG_onimport_entries" "TG_onimport_kodes")))
(if (not (setq scale (tg_get_scale)))
  (progn
    (alert "����� �������� ������������ ������ ����� ���������������� �������")
    (exit)
 )); end if

(setq params (tg_block_variables nil))
(setq old_ucs (TG_set_UCS "������� ��"))
(setq snap (tg_snap nil))
  
(mapcar (function (lambda (x / name position attr tmp)

		    	(setq name (tg_key_match (vl-princ-to-string (nth 4 x)) keys))
		        (if (not (member name x-blocks)) (setq x-blocks (append x-blocks (list name))))
		    	(setq position (list (nth 1 x)(nth 2 x)(* -1 (nth 3 x))))
			(setq attr (append attr (list (list "NUM" (nth 0 x)))))
		    	(if (setq tmp (vl-princ-to-string (nth 4 x)))
			  (progn
				   (setq attr (append attr (list (list "KOD" tmp))))
				   (if (not (member tmp x-kodes)) (setq x-kodes (append x-kodes (list tmp))))
			  ));end progn if
		        (if (setq tmp (nth 3 x)) (setq attr (append attr (list (list "OTM" (if (= CS_type 1) (* tmp -1) tmp))))))
		        (if (setq tmp (nth 5 x)) (setq attr (append attr (list (list "DATE" tmp)))))
			(setq entrys (append entrys (list (tg_insert_block name position ucs scale attr

							(caddar (vl-remove-if-not (function (lambda (x) (= name (cadr x)))) keys))	

							    ))))	    
		    )) inp_file)
(if onimport_name (TG_X-record_save "TG_onimport" onimport_name (mapcar (function (lambda (x) (cons 300 (cdr (assoc 5 (entget x)))))) entrys)))
(tg_x-record_save "TG_onimport_entries" "TG_onimport_blocks" (mapcar (function (lambda (x) (cons 300 x))) x-blocks))
(tg_x-record_save "TG_onimport_entries" "TG_onimport_kodes"  (mapcar (function (lambda (x) (cons 300 x))) x-kodes))

(tg_block_variables params)
(if old_ucs (TG_set_UCS old_ucs))
(tg_snap snap)

(princ)  
  
);end defun

(defun tg_atr_otm_two ( count / ss)
;;;������� ������� ��������� �������� �������� OTM
;;;�� count ������ ����� �������
(if (/= 'INT (type count)) (exit))
(mapcar (function (lambda (x / ent val)
			(if (= (type (setq ent (cadr x))) 'ENAME)
			(progn
			  (setq ent
				(car (vl-remove-if-not (function (lambda (y)

								(= (cdr (assoc 2 y)) "OTM")

							      ))
				  (tg_get_following_data ent)))

				 )
				(if ent
				  (if (/= (setq val (atof (cdr (assoc 1 ent)))) 0)
					    (tg_entmod (cdr (assoc -1 ent)) 1 (rtos val 2 count)));end if
				  
			))
			  )
			  )
		    )
	(ssnamex (ssget "_X" (list (cons 0 "INSERT") (cons 66 1)))))
);end defun

(defun tg_change_block_by_otm (otm_atr / ss)
;;;������� ������ ���������� Z �����
;;;�� �������� �������� � ������ otm_atr.

(mapcar
  (function (lambda (ent / val tmp)
	      (if (= (type ent) 'ename)
		(progn
		  (setq  tmp (cdr
			        (assoc 1
				  (car(vl-remove-if-not
				      (function (lambda (x) (= (cdr (assoc 2 x)) otm_atr)))
				      (tg_get_following_data ent)
	                      ))))
		  );_setq
		  (if tmp (setq var (atof tmp)) (setq var 0))
		  (if (/= var 0)
		    (progn
		      (setq tmp (cdr (assoc 10 (entget ent)))
			    tmp (list (nth 0 tmp) (nth 1 tmp) var)
		      );_setq
		      (tg_entmod ent 10 tmp)
		    ))

		)

		)
	      ))
  (mapcar 'cadr (ssnamex (ssget "_X" (list (cons 0 "INSERT") (cons 66 1))))))
  (princ)
);end defun

(defun tg_less_otm_optim (bl_name otm_atr koef_L koef_H / head tail scale)

;;;����������� ������ �������� �������
;;;������� ��������� �� ���� invis ������, �������
;;;  ��������� �� ���������� ����� 20*scle*koef_L  �
;;;  �������� �������		0.2*scle*koef_h �
;;;bl_name		-��� ������ ������� ����������� �� ������� ������������
;;;otm_atr		-��� �������� �����, ������� �������� ������� ������
;;;koef_L		-����������� ���������� (����� ���� �������� ��� ������������� �������� ������ �������)
;;;koef_H  	-����������� �������    (����� ���� �������� � ������ ������������ �������)
  
(tg_activex_start)
(setq scale (tg_get_scale)
      tail  (tg_enames_from_ss_blocks (list bl_name) (ssget "_X" (list (cons 0 "INSERT") (cons 66 1)))))
(setq tail
	(mapcar (function (lambda (x) (list
				(cdr (assoc -1 x))
				(tg_plan_coordinates (cdr (assoc 10 x)))
				(atof (cdr (assoc 1 (car (vl-remove-if-not (function (lambda (x)(= otm_atr (cdr (assoc 2 x))))) (tg_get_following_data (cdr (assoc -1 x))))))))
			     ))) (mapcar 'entget tail)))

(while (and (setq head (car tail)) (setq tail (cdr tail)))


  (vl-remove-if (function (lambda (x)
			    (if
			      (and
				(< (distance (cadr head) (cadr x))  (* 20 scale koef_L) )
				(< (abs (- (caddr head) (caddr x))) (* 0.2 scale koef_H) )
			      );_and
			      (progn
				(tg_entmod (car x) 8 "invis")
				T
			      )
			      nil
			      );_if
			    )) tail)

);_while  
  
(princ)

);_defun