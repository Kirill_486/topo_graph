(defun TG_get_ucs ( / tg_ucs_record result)

;;;������� ������� ��� ������� ��������� � �����
;;;�������� �� �� ������� ���������������
;;;� ���������� �� � ������� ((��� (10.) (11.) (12.) ������������� ��� nil)......)

(setq tg_ucs_record (tblnext "UCS" t))
(while (/= tg_ucs_record nil)
(setq tg_ucs_record (tg_get_DXF_pairs '(-1 10 11 12 2) tg_ucs_record))
(setq result (cons tg_ucs_record result))  
(setq tg_ucs_record (tblnext "UCS"))
  );end while
(setq result (mapcar (function (lambda (x) (append (list(assoc 2 x)) (tg_ucs_compliment_pair14 (cdr (assoc 2 x)))))) result))  
(setq result (vl-remove-if-not (function (lambda (x)
					   (or
					     (equal (cdr(assoc 14 x)) '(0.0 0.0 1.0) 0.0000001)
					     (equal (cdr(assoc 14 x)) '(0.0 0.0 -1.0) 0.0000001))
					   )) result))
(setq result (mapcar (function (lambda (x / res)
				 (if (equal (cdr(assoc 14 x)) '(0.0 0.0 1.0) 0.0000001) (setq res(append x (list (cons 15 1))) ))
				 (if (equal (cdr(assoc 14 x)) '(0.0 0.0 -1.0) 0.0000001) (setq res (append x (list (cons 15 0)))))
				 res
			)) result ))

);end defun



(defun TG_set_UCS (ucs / old_ucs)
;;;
;;;��������� ������������� ������� �� �� �� � ������ UCS
;;;� ������ ���������� ��������� ������������� ������� ��
;;;���������� ��� ������� ��, ���� ��� ���������
(tg_activex_start)
(if (not curent_doc) tg_ActiveX_start)
(if (not (vl-catch-all-error-p (setq old_ucs (vl-catch-all-apply 'vla-get-activeUCS (list curent_doc)))))
	  (setq old_ucs (cdr
			(assoc 2(entget(vlax-vla-object->ename
					 old_ucs
					)
				 )
			 )))
		(setq old_ucs nil);else
	  );end if
  
 (if (setq ucs (tblobjname "UCS" ucs))
	(progn
	  	(vla-put-ActiveUCS curent_doc (vlax-ename->vla-object ucs))
	  );end progn

   (if (zerop (getvar 'WORLDUCS) )(command "_UCS" ""));else

   );end if 
 old_ucs 
);end defun




(defun tg_vector_multiple (vec1 vec2)

;*    ��������� ������������ ���� ��������
;*    ������� - ����������� 3-������ ������, �������� �� ������������

  (list (- (* (cadr vec1) (caddr vec2))
           (* (caddr vec1) (cadr vec2))
           ) ;_ end of -
        (- (* (caddr vec1) (car vec2))
           (* (car vec1) (caddr vec2))
          ) ;_ end of -
        (- (* (car vec1) (cadr vec2))
           (* (cadr vec1) (car vec2))
           ) ;_ end of -
        ) ;_ end of list
  ) ;_ end of defun



(defun tg_ucs_compliment_pair14 (ucs / kod14)
;;;
;;;��������� ������ � ������� ��������� (���� 10 11 12)
;;;����� 14, ���������� ��������� ������ ��� Z
;;;
  (if (setq ucs (tblobjname "UCS" ucs))
    (progn
	(setq ucs (append (tg_get_DXF_pairs '(10 11 12) (entget ucs))
			  (list (cons 14
				(tg_vector_multiple (cdr (assoc 11 (entget ucs)))(cdr (assoc 12 (entget ucs))))))))
	);end prodn
  );end if
);end defun


(defun tg_matrix_multipline (matrix values / i str g gmax val)
;;; �������� ���������� ������� �� ������� �� ������ ������ 
;;;     m1 m2 m3  -   v1
;;;     m4 m5 m6  -   v2
;;;     m7 m8 m9  -   v3
;;; ����������� ������� (�� ����)
(if (= (length matrix) (expt (length values) 2))
(progn
(setq i -1)  
(while (< (setq i (1+ i)) (length values))
 (setq g (1- (* i (length values))) gmax (* (1+ i) (length values)))
 (while (< (setq g (1+ g)) gmax) (setq str (append str (list (* (nth g matrix) (nth i values))))))
 (setq val (append val (list str)) str nil)
);end while
(mapcar (function (lambda (x y z) (+ x y z))) (nth 0 val)(nth 1 val)(nth 2 val) )
);end progn
);end if
);end defun


(defun tg_trans_to_ucs (point ucs / ucs_bp tmp)
;;;��������� ���������� ����� �� ������� ������� ��������� � ����������������
(if (setq ucs (tg_ucs_compliment_pair14 ucs))
(progn
 (setq ucs_bp (cdr (assoc 10 ucs)))
 (setq point (mapcar (function (lambda (x y) (- x y))) point ucs_bp))
 (setq ucs (mapcar (function (lambda (x y z) (list x y z))) (cdr (assoc 11 ucs)) (cdr (assoc 12 ucs)) (cdr (assoc 14 ucs))))
 (setq point (tg_matrix_multipline (append (nth 0 ucs)(nth 1 ucs)(nth 2 ucs)) point))
);end progn
);end if  
);end defun


(defun tg_trans_from_ucs (point ucs / ucs_bp tmp)
;;;��������� ���������� ����� �� ���������������� ������� ��������� � �������
(if (setq ucs (tg_ucs_compliment_pair14 ucs))
(progn
 (setq ucs_bp (cdr (assoc 10 ucs)))
 (setq ucs (mapcar (function (lambda (x y z) (list x y z))) (cdr (assoc 11 ucs)) (cdr (assoc 12 ucs)) (cdr (assoc 14 ucs))))
 (setq ucs (mapcar (function (lambda (x y z) (list x y z))) (nth 0 ucs)(nth 1 ucs)(nth 2 ucs) ))
; (setq ucs (append (cdr (assoc 11 ucs)) (cdr (assoc 12 ucs)) (cdr (assoc 14 ucs))))
 (setq point (tg_matrix_multipline (append (nth 0 ucs)(nth 1 ucs)(nth 2 ucs)) point))
 (setq point (mapcar (function (lambda (x y) (+ x y))) point ucs_bp))
);end progn
point
);end if  

);end defun


(defun tg_trans_from_UCS_to_UCS (point ucs_from ucs_to /)

;;;����������� ���������� �� ucs_from
;;;� ucs_to
;;;
(if (null ucs_from) (setq ucs_from "������� ��"))
(if (null ucs_to) (setq ucs_to "������� ��"))

(cond
  ((and (tblobjname "UCS" ucs_from) (tblobjname "UCS" ucs_to))
	(tg_trans_to_ucs (tg_trans_from_ucs point ucs_from) ucs_to)
   )
  ((tblobjname "UCS" ucs_from) (tg_trans_from_ucs point ucs_from))
  ((tblobjname "UCS" ucs_to)   (tg_trans_to_ucs point ucs_to))
  (T point)
);end cond
);end defun

