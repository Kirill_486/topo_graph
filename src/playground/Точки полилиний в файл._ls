(defun TG_Get_Poly_Points (obj / res obj_part)
;������ ������ ��������� �����-������

  (setq rec (lambda (name / n_a)
    (progn (setq n_a (entget name))  
       (cond
	 ( (= (cdr(assoc 0 n_a)) "VERTEX")
	   (append (list (assoc 10 n_a)) (apply (function rec) (list (entnext name))))
	); end vertex
          ( (= (cdr(assoc 0 n_a)) "SEQEND") '(nil)); end seqend
        ); end cond
     ); end progn
    );end lambda
  ); end setq
(setq obj_part (entget obj))
(cond
   ((= (cdr(assoc 0 obj_part)) "LWPOLYLINE")); end lwpoly
   ((= (cdr(assoc 0 obj_part)) "LINE") (setq obj_part (append (list(assoc 10 obj_part)) (list(assoc 11 obj_part)))))
   ((= (cdr(assoc 0 obj_part)) "POLYLINE")   
    (setq obj_part (apply (function rec) (list (entnext obj))))     
   ); end poly
     (t "");end t
); end cond
     (setq obj_part (mapcar 'cdr (vl-remove-if-not '(lambda (x) (or (= (car x) 10) (= (car x) 11))) obj_part))
     (mapcar (function (lambda (x) (trans x 1 0))) obj_part)
); end defun


(defun TG_put_points_to_file (coor res_file / )

;;;��������� ������� ������������ ���� �� ������ ����� 
;;;coor-������ �����       
(mapcar

	(function

		(lambda (x / str)

		  (setq x (vl-princ-to-string x))
		  (setq x (substr x 2 (- (strlen x) 2)))
		  (write-line x res_file)
		  );end lambda
	  );end func
	coor
  ); end mapcar
 );end defun



(defun tg_get_points_from_Pline_to_file (/ tg_ss res_file)
;;;��������� ��������� ������� ��������� � ��� ����
;;;
;;;tg_ss ��� ������ � �����������
(if (not (setq tg_ss (ssget))) (exit))
(setq tg_ss (cdr (reverse (ssnamex tg_ss))))
(setq tg_ss (mapcar 'cadr tg_ss))
(setq tg_ss (mapcar 'TG_Get_Poly_Points tg_ss))
(if (not (setq res_file (open (getfiled "�������������� ����" "" "krd" 5) "w"))) (exit))


(mapcar (function (lambda (elem)

	(mapcar (function (lambda (coor)

			    (setq coor (vl-princ-to-string coor))
			    (setq coor (substr coor 2 (- (strlen coor) 2)))
			    (write-line coor res_file)

		    );end lambda coor
		  );end function coor
	elem );end mapcar coor

		    );end mapcar lambda elem



		  ) tg_ss) ; end mapcar ss

(close res_file)
(close res_file)
);end defun