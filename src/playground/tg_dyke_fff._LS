;;;��������� ������ ������. ��������� ��� ��������� �������.
;;;1. �������� ���� ��
;;;2. �������� ���� ��
;;;3. ���� ���� 3-������ - �������� �� �� 2-������
;;;4. �������� ����� �� 1 (��������� ��������� � 2-�)
;;;5. �������� ����� �� 2 (��������� ��������� � 2-�)
;;;6. �������� ����� �� 1 (��������� ��������� � 2-�)
;;;7. �������� ����� �� 2 (��������� ��������� � 2-�)
;;;8. ������� ����� �� ������
;;;9. ������� ����� �� ��������
;;;10. ���������
;;;11. ����� �� ���������� ������� ��

;;;(defun tg_reverce_poly)

(defun tg_lines_draw (from_list to_list is_dot small_shtr / small lines)
;;;������� ������ ����� ������
;;;from_list  - ������ ��������� ����� ������
;;;to_list    - ������ ��������� ����� ��������
;;;is_dot     - ������� �������������
;;;small_shtr - ����� ���������� ������ �� ������
(setq snap (tg_snap nil)
      from_list (cdr from_list)
      to_list   (cdr to_list)
      )

(if is_dot (setq small_shtr (/ small_shtr 1.5)))
  
(mapcar (function (lambda (x y)
  			(if (and x y)
			  (progn
		             (if
			       (setq small (not small))
			       (vl-cmdf "_LINE" x y "")
			     )
			     (if
			       (null small)
			       (progn
				 (if (< (setq tmp (distance x y)) (* 1.5 small_shtr))
				       (setq ttmp (/ tmp 2))
				       (setq ttmp small_shtr)
  				 );_if
				 (vl-cmdf "_LINE" x (polar x (angle x y) ttmp) "")
				 (if is_dot (vl-cmdf "_LINE"
					      (polar x (angle x y) (* 1.3 ttmp))
					      (polar x (angle x y) (* 1.5 ttmp))
					      "")
				  );_if
			       )
			     )
   			   (setq lines (append lines (list (entlast))))
			 );_progn
			);_if
		    ))
	from_list
	to_list
	)
(tg_snap snap) 
lines
);_defun


(defun tg_3d_pline_change (3d_poly is_reversed / coor points i razm)
;;;�������� 3D ��������� �� 2D
;;;3d_poly      - ActiveX ��������� �� 3� ���������
;;;is_reversed  - �������� ����������� ����� ��� ���
;;;razm         - ����� ������� ����� ����� ���������
;;;coor         - �������� �������� Coordinates �������� �����
;;;points       - ������ �� ��������� 3-� ������ ����� ��� �������� ����� ��
;;;i            - �������
(setq coor (vlax-safearray->list (vlax-variant-value (vla-get-Coordinates 3d_poly))))
(setq i -3)
(repeat (/ (length coor) 3)
 (setq i (+ 3 i))
 (setq points (append points (list (list (nth i coor) (nth (1+ i) coor) 0))))
);_repeat
(if is_reversed (setq points (reverse points)))
(setq razm (cons 0 (1- (length coor)) ))
(setq 3d_poly (vla-Add3DPoly (vla-get-ModelSpace curent_doc)
                 (vlax-safearray-fill
		   (vlax-make-safearray vlax-vbDouble razm) (apply 'append points))))
(vla-put-Color 3d_poly 6)
3d_poly
);_defun


(defun tg_slice_for_dike (pline1;������ �����
			  pline2;������ �����
			  pt1st ;����� ������ 1
			  pt2st ;����� ������ 2
			  pt1ed ;����� ����� 1
			  pt2ed ;����� ����� 2
			  dist  ;���������� ����� ��������
			  /
			  p_from;����� ������
			  p_to  ;����� ��������
			  p1    ;��������� �����
			  p2    ;�������� �����
			  tmp   ;���������
			  
			  )
;;;������� �������� ��������� �� ������ ������� �� ����� ������
;;;�� ����� �����.
;;;��������� ������ ���� ��� ������������.
;;;�� ������ - ������� ��������� ����� ������ � ��������
(setq points1 (vl-sort (list
	                 (if (setq tmp (vlax-curve-getDistAtPoint
					 pline1
					 (vlax-curve-getClosestPointToProjection
					   pline1
					   (tg_plan_coordinates pt1st)
					   '(0 0 1)
					   "")
					 )
				   )
			       tmp
			       0)
                         (if (setq tmp (vlax-curve-getDistAtPoint
					 pline1
					 (vlax-curve-getClosestPointToProjection
					   pline1
					   (tg_plan_coordinates pt1ed)
					   '(0 0 1)
					   "")
					 )
				   )
			       tmp
			       (vla-get-Length pline1))

	               );_list
		       '<
		    );_sort
      points2 (vl-sort (list
	                 (if (setq tmp (vlax-curve-getDistAtPoint
					 pline2
					 (vlax-curve-getClosestPointToProjection
					   pline2
					   (tg_plan_coordinates pt2st)
					   '(0 0 1)
					   "")
					 )
				   )
			       tmp
			       0)
                         (if (setq tmp (vlax-curve-getDistAtPoint
					 pline2
					 (vlax-curve-getClosestPointToProjection
					   pline2
					   (tg_plan_coordinates pt2ed)
					   '(0 0 1)
					   "")
					 )
				   )
			       tmp
			       (vla-get-Length pline1))

                        )
		       '<
		    );_sort
      );_setq

(setq tmp (car points1) ttmp nil count 0)
(while (< (setq tmp (+ tmp dist)) (cadr points1))
(setq ttmp (append ttmp (list tmp))
      count (1+ count)
    );_setq
);_while
(setq points1 (append
		(list (car points1))
		ttmp
		(cdr points1)
		)
      )

(setq dist (abs (/ (- (car points2) (cadr points2)) count)))

(setq tmp (car points2) ttmp nil count 0)
(while (< (setq tmp (+ tmp dist)) (cadr points2))
(setq ttmp (append ttmp (list tmp))
      count (1+ count)
    );_setq
);_while
(setq points2 (append
		(list (car points2))
		ttmp
		(cdr points2)
		)
      );_setq

(setq points1 (mapcar (function (lambda (x) (vlax-curve-getPointAtDist pline1 x))) points1))
(setq points2 (mapcar (function (lambda (x) (vlax-curve-getPointAtDist pline2 x))) points2))
(list points1 points2)  
);_defun


(defun tg_ask_2_points (prefix / p1 p2)
;;;������� ������ � ������������ ������� 2 �����
;;;����������� ������ ������ �� ���.
(princ "\n")
(princ (strcat "������� ����� 1 " prefix))
(setq p1 (getpoint))
(princ "\n")
(princ (strcat "������� ����� 2 " prefix))
(setq p2 (getpoint))
(list p1 p2)
);_defun



(defun tg_get_dist_at_point (pline point / )
(vlax-curve-getDistAtPoint
  pline
  (vlax-curve-getClosestPointToProjection
	pline
	point
	'(0 0 1)
	""
  )
)

);end defun


(defun tg_dyke (scale		;�������
		is_dot		;������� �������������
		small_shtr	;����� ������ ������
		/		;
		pline1		;������
		pline2		;��������
		wpl1		;������ ��� ������
		wpl2		;�������� ��� ������
		lines		;�������
		prog_end	;������� ��������� ��������� �������
		pstart		;����� ����� �������
		ptend		;����� ����� �������
		)
;;;�������� ����������� ������� ���������� �������
;;;1. ���������� 2 �����
;;;2. ��������� �� �� "���������"
;;;3. �������� ��� ������������� ��������� ����� �������� (��������� � entdel)
;;;4. ����������� ���� ����� ������
;;;5. ����������� ���� ����� �����
;;;6. ���������� �� ������� ��������� �����, ������ �������, ���������� �������
(tg_activex_start)
(princ "\n")
(princ "pline1")
(setq pline1 (vlax-ename->vla-object(car (entsel))))
(princ "\n")
(princ "pline2")
(setq pline2 (vlax-ename->vla-object(car (entsel))))
(princ "\n")

(setq pstart (tg_ask_2_points "������ ������"))
(setq ptend (tg_ask_2_points "����� ������"))
(if (> 0 (-
	   (tg_get_dist_at_point pline1 (car ptend))
	   (tg_get_dist_at_point pline1 (car pstart))))
  (setq wpl1 (tg_3d_pline_change pline1 t))
  (setq wpl1 (tg_3d_pline_change pline1 nil))
);_if 
(if (> 0 (-
	   (tg_get_dist_at_point pline2 (cadr ptend))
	   (tg_get_dist_at_point pline2 (cadr pstart))))
  (setq wpl2 (tg_3d_pline_change pline2 t))
  (setq wpl2 (tg_3d_pline_change pline2 nil))
);_if 
(vla-put-Visible pline1 :vlax-false)
(vla-put-Visible pline2 :vlax-false)

(while (not prog_end)
(gc)
(setq points (tg_slice_for_dike wpl1 wpl2 (car pstart) (cadr pstart) (car ptend) (cadr ptend) scale))
(setq points
       (list
	       
       (mapcar (function (lambda (x)
			   (vlax-curve-getClosestPointToProjection
	                     pline1
	                     x
	                     '(0 0 1)
	                      nil
                           )
			  )
		);_func
	       (car points)
	   );_mapcar
       (mapcar (function (lambda (x)
			   (vlax-curve-getClosestPointToProjection
	                     pline2
	                     x
	                     '(0 0 1)
	                      nil
                           )
			  )
		);_func
	       (cadr points)
	   );_mapcar
       );_list
     );_setq
  (setq lines (append lines (tg_lines_draw (car points) (cadr points) is_dot small_shtr)))

  (setq pstart ptend)
  (tg_try
    (function
      (lambda nil
	(setq ptend (tg_ask_2_points "����� ������")
	      
	      )
	)
      ) '(progn (setq prog_end t) (tg_dyke_restore lines pline1 pline2 wpl1 wpl2))
    );_try

  

);_while

);_defun



(defun tg_dyke_restore (lines	;������� (������� ������)
			pl1	;�������� ����1
			pl2	;�������� ����2(������� ���������)
			wpl1	;������� ���� 
			wpl2	;������� ���� (�������)
			/
			ss	;����� �� ��������
			)

(vla-put-Visible pl1 :vlax-true)
(vla-put-Visible pl2 :vlax-true)
(vla-Delete wpl1)
(vla-Delete wpl2)
(setq ss (ssadd))
(mapcar (function (lambda (x) (ssadd x ss))) lines)
(sssetfirst nil ss)
(VL-CMDF "_.Group")
(VL-CMDF "")
  

);_defun