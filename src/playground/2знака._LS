(defun tg_atr_otm_two ( count / ss)
;;;������� ������� ��������� �������� �������� OTM
;;;�� count ������ ����� �������
(if (/= 'INT (type count)) (exit))
(mapcar (function (lambda (x / ent val)
			(if (= (type (setq ent (cadr x))) 'ENAME)
			(progn
			  (setq ent
				(car (vl-remove-if-not (function (lambda (y)

								(= (cdr (assoc 2 y)) "OTM")

							      ))
				  (tg_get_following_data ent)))

				 )
				(if ent
				  (if (/= (setq val (atof (cdr (assoc 1 ent)))) 0)
					    (tg_entmod (cdr (assoc -1 ent)) 1 (rtos val 2 count)));end if
				  
			))
			  )
			  )
		    )
	(ssnamex (ssget "_X" (list (cons 0 "INSERT") (cons 66 1)))))
);end defun

(defun tg_change_block_by_otm (otm_atr / ss)
;;;������� ������ ���������� Z �����
;;;�� �������� �������� � ������ otm_atr.

(mapcar
  (function (lambda (ent / val tmp)
	      (if (= (type ent) 'ename)
		(progn
		  (setq  tmp (cdr
			        (assoc 1
				  (car(vl-remove-if-not
				      (function (lambda (x) (= (cdr (assoc 2 x)) otm_atr)))
				      (tg_get_following_data ent)
	                      ))))
		  );_setq
		  (if tmp (setq var (atof tmp)) (setq var 0))
		  (if (/= var 0)
		    (progn
		      (setq tmp (cdr (assoc 10 (entget ent)))
			    tmp (list (nth 0 tmp) (nth 1 tmp) var)
		      );_setq
		      (tg_entmod ent 10 tmp)
		    ))

		)

		)
	      ))
  (mapcar 'cadr (ssnamex (ssget "_X" (list (cons 0 "INSERT") (cons 66 1))))))
  (princ)
);end defun

(defun tg_block_if_must_hide (blk1 blk2 dist dz lay otm_atr / otm1 otm2 pos1 pos2 ans)
;;;������� ��������� 2 ����� �� �������
;;;  ���������� �� �� ���������� �����         dist
;;;  ������� �� ������� ����� (otm_atr)    dz
;;;��������� ���� 2 �� ���� lay � ���������� ��� ���
;;;���������� ������ ���������� nil (���������� ans)
;;;���� ������� ��� ��� ��� 0 - ���������� nil (���������� ans)

(setq otm1
       (cdr
	 (assoc 1
            (car (vl-remove-if-not
	 	 (function (lambda (x)
		     (= otm_atr (cdr (assoc 2 x)))
		     ))
	 	(tg_get_following_data blk1)
	 	))
	)
  )
      otm2
       (cdr
	 (assoc 1
            (car(vl-remove-if-not
	 	 (function (lambda (x)
		     (= otm_atr (cdr (assoc 2 x)))
		     ))
	 	(tg_get_following_data blk2)
	 	))
	)
  )
     pos1 (tg_plan_coordinates (cdr (assoc 10 (entget blk1))))
     pos2 (tg_plan_coordinates (cdr (assoc 10 (entget blk2))))
 );_setq
 (if (and otm1 otm2 (/= 0 (setq otm1 (atof otm1))) (/= 0 (setq otm2 (atof otm2))))
     (progn
       (if (and (< (distance pos1 pos2) dist) (< (abs (- otm1 otm2)) dz) )
           (progn (setq ans blk2) (tg_entmod blk2 8 lay))
	 )
     )
   );_if
  ans
);_defun


(defun tg_less_points (otm_atr / scale head tail)
;;;������� ��������� ��� ����� � ���������� ���� � ��� ���� ������� ��
;;;������������.
;;;���������� ����� 25*�������
;;;������� ������� ����� 0.2*������� 
(setq scale (tg_get_scale)
      tail (mapcar 'cadr (ssnamex (ssget "_X" (list (cons 0 "INSERT") (cons 66 1)))))
);_setq

(while (and (setq head (car tail)) (setq tail (cdr tail)))

  (setq tail
	 (vl-remove-if (function (lambda (x)
				   (tg_block_if_must_hide
				       head
				       x
				       (* 20 scale)
				       (* 0.2 scale)
				       "invis"
				       otm_atr
				     )
				   )) tail);_ri
  );_setq 

);_while
  
);_defun



(defun tg_less_otm_optim (bl_name otm_atr / head tail scale)
(tg_activex_start)
(setq scale (tg_get_scale)
      tail  (tg_enames_from_ss_blocks (list bl_name) (ssget "_X" (list (cons 0 "INSERT") (cons 66 1)))))
(setq tail
	(mapcar (function (lambda (x) (list
				(cdr (assoc -1 x))
				(tg_plan_coordinates (cdr (assoc 10 x)))
				(atof (cdr (assoc 1 (car (vl-remove-if-not (function (lambda (x)(= otm_atr (cdr (assoc 2 x))))) (tg_get_following_data (cdr (assoc -1 x))))))))
			     ))) (mapcar 'entget tail)))

(while (and (setq head (car tail)) (setq tail (cdr tail)))


  (vl-remove-if (function (lambda (x)
			    (if
			      (and
				(< (distance (cadr head) (cadr x))  (* 20 scale) )
				(< (abs (- (caddr head) (caddr x))) (* 0.2 scale) )
			      );_and
			      (progn
				(tg_entmod (car x) 8 "invis")
				T
			      )
			      nil
			      );_if
			    )) tail)

);_while  
  
(princ)

);_defun y