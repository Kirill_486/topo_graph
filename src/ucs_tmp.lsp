(defun det_4 (matrix /)
;������� ������������ ������� 2�2
;
;
(if (= (length matrix) 4)
(- (* (nth 0 matrix) (nth 3 matrix)) (* (nth 1 matrix) (nth 2 matrix)))  
);end if
);end defun


(defun tg_vector_multiple (vec1 vec2)

;*    ��������� ������������ ���� ��������
;*    ������� - ����������� 3-������ ������, �������� �� ������������

  (list (- (* (cadr vec1) (caddr vec2))
           (* (caddr vec1) (cadr vec2))
           ) ;_ end of -
        (- (* (caddr vec1) (car vec2))
           (* (car vec1) (caddr vec2))
          ) ;_ end of -
        (- (* (car vec1) (cadr vec2))
           (* (cadr vec1) (car vec2))
           ) ;_ end of -
        ) ;_ end of list
  ) ;_ end of defun



(defun tg_ucs_compliment_pair14 (ucs / kod14)
;;;
;;;��������� ������ � ������� ��������� (���� 10 11 12)
;;;����� 14, ���������� ��������� ������ ��� Z
;;;
  (if (setq ucs (tblobjname "UCS" ucs))
    (progn
	(setq ucs (append (tg_get_DXF_pairs '(10 11 12) (entget ucs))
			  (list (cons 14
				(tg_vector_multiple (cdr (assoc 11 (entget ucs)))(cdr (assoc 12 (entget ucs))))))))
	);end prodn
  );end if
);end defun


(defun tg_matrix_multipline (matrix values / i str g gmax val)
;;; �������� ���������� ������� �� ������� �� ������ ������ 
;;;     m1 m2 m3  -   v1
;;;     m4 m5 m6  -   v2
;;;     m7 m8 m9  -   v3
;;; ����������� ������� (�� ����)
(if (= (length matrix) (expt (length values) 2))
(progn
(setq i -1)  
(while (< (setq i (1+ i)) (length values))
 (setq g (1- (* i (length values))) gmax (* (1+ i) (length values)))
 (while (< (setq g (1+ g)) gmax) (setq str (append str (list (* (nth g matrix) (nth i values))))))
 (setq val (append val (list str)) str nil)
);end while
(mapcar (function (lambda (x y z) (+ x y z))) (nth 0 val)(nth 1 val)(nth 2 val) )
);end progn
);end if
);end defun


(defun tg_trans_to_ucs (point ucs / ucs_bp tmp)
;;;��������� ���������� ����� �� ������� ������� ��������� � ����������������
(if (setq ucs (tg_ucs_compliment_pair14 ucs))
(progn
 (setq ucs_bp (cdr (assoc 10 ucs)))
 (setq point (mapcar (function (lambda (x y) (- x y))) point ucs_bp))
 (setq ucs (mapcar (function (lambda (x y z) (list x y z))) (cdr (assoc 11 ucs)) (cdr (assoc 12 ucs)) (cdr (assoc 14 ucs))))
 (setq point (tg_matrix_multipline (append (nth 0 ucs)(nth 1 ucs)(nth 2 ucs)) point))
);end progn
);end if  
);end defun


(defun tg_trans_from_ucs (point ucs / ucs_bp tmp)
;;;��������� ���������� ����� �� ������� ������� ��������� � ����������������
(if (setq ucs (tg_ucs_compliment_pair14 ucs))
(progn
 (setq ucs_bp (cdr (assoc 10 ucs)))
 (setq ucs (mapcar (function (lambda (x y z) (list x y z))) (cdr (assoc 11 ucs)) (cdr (assoc 12 ucs)) (cdr (assoc 14 ucs))))
 (setq ucs (mapcar (function (lambda (x y z) (list x y z))) (nth 0 ucs)(nth 1 ucs)(nth 2 ucs) ))
; (setq ucs (append (cdr (assoc 11 ucs)) (cdr (assoc 12 ucs)) (cdr (assoc 14 ucs))))
 (setq point (tg_matrix_multipline (append (nth 0 ucs)(nth 1 ucs)(nth 2 ucs)) point))
 (setq point (mapcar (function (lambda (x y) (+ x y))) point ucs_bp))
);end progn
);end if  

);end defun