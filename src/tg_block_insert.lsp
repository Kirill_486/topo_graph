;;;(defun tg_insert (name coord scale ang vis lay)
;;;;;;������ ������� ������� �����
;;;;;;��������� ���� � ����� � ������������ coord
;;;;;;                  ��������� ��������� vis
;;;;;;                              ������� scale ��� �������� nil ����� �� tg_scale
;;;;;;                        ���� �������� ang
;;;;;;                                 ���� lay
;;;
;;;);defun


(defun tg_simple_block_insert (block r_ang / scale ucs params snap cont comm)
(if command-s (setq comm command-s) (setq comm vl-cmdf))
(tg_load_block_library (list block))
(setq ucs (TG_set_UCS "")
      params (tg_block_variables nil)
      snap (tg_snap nil)
      cont T
);_setq

(if  (null (setq scale (tg_get_scale))) (setq scale 1))
	
  (while cont

	(if r_ang
	  (setq r_ang
		 (angtos
		   (* (atoi (vl-list->string
				(list (last (vl-string->list (rtos (getvar 'cdate) 2 10))))
			)) (/ pi 10) )
		   0))
	 )

	(tg_try
	  (function (lambda nil (comm "_insert" block pause scale (if r_ang r_ang 0))))
          '(progn
	    (tg_block_variables params)
	    (if ucs (TG_set_UCS ucs))
	    (tg_snap snap)
	    (setq cont nil)
	   )
	  )

  );_while

        
);end defun

(defun tg_block_insert_3_pt (block / p1 p2 p3 ucs plan_ucs p_perp tg_x
		     		tg_y tg_ang tg_ang_dop d1 d2 d params snap) 
(tg_load_block_library (list block))
(setq ucs (TG_set_UCS ""))

(initget 1)

(setq p1 (tg_plan_coordinates (getpoint "������ �����")))
(setq p2 (tg_plan_coordinates (getpoint p1 "������ �����")))
(setq p3 (tg_plan_coordinates (getpoint p2 "������ �����")))
(setq params (tg_block_variables nil))
(setq snap (tg_snap nil))

(setq tg_x (distance p1 p2))

(setq tg_ang (+ (angle p1 p2)  (/ pi 2)))

(setq p_perp (polar p3 tg_ang 100))
(setq p_perp (inters p1 p2 p3 p_perp nil))
(setq tg_y (distance p3 p_perp))
  
(setq d1 (distance p1 p_perp))
(setq d2 (distance p2 p_perp))

(if (> d2 d1)
  (progn
    
     (setq p_perp p2)
     (setq p2 p1)
     (setq p1 p_perp)
  ); end progn
 ); end if
  
(setq tg_x (- 0 tg_x))
(setq tg_ang (+ (angle p1 p2) (/ pi 2)))
(setq tg_ang (angtos tg_ang 0))
(setq d (-(* (- (nth 0 p3) (nth 0 p1)) (-(nth 1 p2) (nth 1 p1)) ) (* (-(nth 1 p3) (nth 1 p1)) (-(nth 0 p2) (nth 0 p1)) )))
(if (< d 0) (setq tg_y (- tg_y)))
(command "_insert" block p1 tg_y tg_x tg_ang)
(tg_block_variables params)
(if ucs (TG_set_UCS ucs))
(tg_snap snap)
);end defun


(defun TG_BLK_Linar (tg_Blk p1 p2 / p position)

; ��������� ���� ��������� ��������-������ �� ����� ���� � ����������

  (setq p (mapcar (function (lambda (x) (list (nth 0 x) (nth 1 x)))) (list p1 p2)))
  (if (< (- (car p2) (car p1)) 0) (setq p (reverse p)))
  
  (setq position (nth 0 p))
  (setq len (distance (nth 0 p) (nth 1 p)))
  (setq ang (angle (nth 0 p) (nth 1 p)))
  
  (if (not curent_doc) tg_ActiveX_start)
  (command "_insert" tg_blk position 1 0)
  (setq tg_blk (entlast))
  (setq tg_blk (vlax-ename->vla-object tg_blk))
  (SetDynamicBlockPropertyNameValue tg_blk "OB_Len" len)
  (SetDynamicBlockPropertyNameValue tg_blk "OB_Ang" ang)
  

); end defun

(defun TG_Insert_Linar_by_Pl (tg_BLK / ucs TG_Pline TG_Pline_ent Points k snap)
; ��������� �������� ������� ���� ��� �� ���������

(tg_load_block_library (list tg_BLK))
(setq ucs (TG_set_UCS ""))
(setq params (tg_block_variables nil))
(setq snap (tg_snap nil))
  
(setq TG_Pline_ent (car (entsel "�������� ������� ���������")))
(setq TG_Pline (TG_Get_Poly_Points TG_Pline_ent))
(entdel TG_Pline_ent)
(setq k 0)
(repeat (1- (length TG_Pline))
   (TG_BLK_Linar tg_Blk (nth k tg_Pline) (nth (1+ k) tg_Pline))
   (setq k (1+ k))
); end repeat

(tg_block_variables params)
(if ucs (TG_set_UCS ucs))
(tg_snap snap)
  
); end defun

(defun tg_insert_crosses (block / p1 p2 ucs scale interval pp places sy params ang snap pls)

(tg_load_block_library (list block))
(if  (null (setq scale (tg_get_scale))) (setq scale 1))
(setq interval (* scale 100))

(initget 0 (strcat "<������� ��>" (tg_list_to_string_space (mapcar (function (lambda (x) (cdr (assoc 2 x)))) (TG_get_ucs)))))
(setq ucs (getkword (strcat "������������� �� <������� ��> " "["
			   (vl-string-translate " " "/"
			     (vl-string-subst "" " "
			       (tg_list_to_string_space
			       (mapcar (function (lambda (x) (cdr (assoc 2 x))))
				       (TG_get_ucs)))
			       )
			     )
		    "] :"  )
		   ))
(if ucs
  (setq ang
	 (angle '(0 0 0)
		(cdr (assoc 11 (car (vl-remove-if-not
				      (function (lambda (x)
						  (= (cdr (assoc 2 x)) ucs))) (TG_get_ucs))))))))

(initget 1)

(setq p1 (tg_trans_from_UCS_to_UCS (trans (tg_plan_coordinates (getpoint "������ �����")) 1 0) nil ucs))
(setq p2 (tg_trans_from_UCS_to_UCS (trans (tg_plan_coordinates (getcorner p1 "������ �����")) 1 0) nil ucs))
(setq pp (list 	(list
		  (TG_min_kratnoe (min (nth 0 p1) (nth 0 p2)) interval )
		  (TG_min_kratnoe (min (nth 1 p1) (nth 1 p2)) interval )
		)
		(list
		  (+ interval (TG_min_kratnoe (max (nth 0 p1) (nth 0 p2)) interval))
		  (+ interval (TG_min_kratnoe (max (nth 1 p1) (nth 1 p2)) interval))
		)
	    ))
(setq pls (car pp) sy (cadr pls))


(repeat (fix(+ 0.5 (/ (- (caadr pp) (caar pp)) interval)))

(repeat (fix(+ 0.5 (/ (- (cadadr pp) (cadar pp)) interval)))

  (setq places (append places (list pls)))
  (setq pls (list (nth 0 pls) (+ interval (nth 1 pls))))

)

  (setq pls (list (+ interval (nth 0 pls)) sy))
  

);end repeat

(setq params (tg_block_variables nil))
(setq plan_ucs (TG_set_UCS ""))
(setq snap (tg_snap nil))
  

(mapcar (function (lambda (x)
		    (vl-cmdf "_insert"
			     block
			     (tg_trans_from_UCS_to_UCS (append x (list 0)) ucs nil)
			     scale
			     0)
		    (if ang (tg_entmod (entlast) 50 ang))
		    )) places)
(if plan_ucs (TG_set_UCS plan_ucs))
;(command "�����������" "�" block)
(if params (tg_block_variables params))
(tg_snap snap)
(tg_change_atr_by_position ucs "conf")
);end defun



(defun tg_refresh_design ( / design plan_parameters)
(setq design (tg_x-record_load "TG_Design" "TG_Design"))
(setq plan_parameters (tg_x-record_load "TG_plan_parameters" "TG_plan_parameters"))

(if plan_parameters

  	(TG_X-record_save "TG_Design" "TG_Design"
			
		 (append plan_parameters
			
			   (vl-remove-if (function (lambda (x)

				(or (= (car x) 300)
				    (= (car x) 301)
				    (= (car x) 71)
				    (= (car x) 41)
				    (= (car x) 305)
				    (= (car x) 306)

				    )
			   )) design)))
)
);end defun

(defun tg_plan_design (block / ucs params snap attr attr_dict)
(tg_refresh_design)
(setq params (tg_block_variables nil))
(setq ucs (TG_set_UCS ""))
(setq snap (tg_snap nil))
(tg_load_block_library (list block))

(vl-cmdf "_insert" block '(0 0 0) 1 0 "")
(tg_block_variables params)
(if ucs(TG_set_UCS ucs))
(tg_snap snap)
(setq attr (tg_get_following_data (entlast)))
);end defun

(defun tg_new_layout (name center wi hi ang / ss scale vport comm)
;;;������� ������� ���� � �� ��� ������� �����.
;;;name - ��� �����
;;;center - ����� � ������ ���� �������
;;;wi - ������ ���������� �� ����� ������� � �������� ������
;;;hi - �� ��, ��� � ����, �� ������
;;;ang - ���� �������� ���� �� ��

(if command-s (setq comm command-s) (setq comm vl-cmdf))

  
(if (member name (layoutlist)) (progn (alert "��� ����� ������ ���� ����������") (exit)))
(vla-add (vla-get-Layouts curent_doc) name)
(setvar 'ctab name)
(if (setq ss (ssget "_X" (list (cons 410 name))))
  (mapcar (function
	    (lambda (x / ent)
	      (if (= 'ename (type (setq ent (cadr x))))
		(entdel ent))))
	  (ssnamex ss)))
(if  (null (setq scale (tg_get_scale))) (setq scale 1))

(setq vport (vla-AddPViewport (vla-get-PaperSpace curent_doc) (vlax-3d-point '(0 0 0)) wi hi))
;;;������ �� � ������ ��������� �����
(vla-put-CustomScale vport (/ 1 scale))
;;;���������� ���������� ����������� ��
(vla-put-TwistAngle vport ang)
;;;���������� ���� �������� ���� � ��
(vla-put-Target vport (vlax-3d-point center))
;;;����������� ����� � ������, �� ������� �������
(vla-put-ViewportOn vport :vlax-true)
;;;������� ��
(comm "_.MSPACE")
(comm "��������" "�" center "" "" "")
(comm "_.PSPACE")

;;;(comm "_regenall")
);end defun

(defun tg_new_layout_by_block (block / ss scale comm)
(tg_ActiveX_start)

(if command-s (setq comm command-s) (setq comm vl-cmdf))
  
(princ "\n �������� ����� ���������:")  
(setq ss (ssget))

(setq ss
  (mapcar (function (lambda (x / list_num base_point)

		(append
		  (vl-remove-if-not (function (lambda (z) (or
							  (= (car z) "WIDTH")
							  (= (car z) "HEIGHT")
							  (= (car z) "ANG")
							  )))
		 (mapcar (function (lambda (y)

				    (cons
					(vlax-get-property y 'PropertyName)
					(vlax-variant-value (vlax-get-property y 'Value))
					)
				    ) )

		   (progn
		     (setq list_num (cdr (assoc 1
				     (car(vl-remove-if-not (function (lambda (p)
					(and (= (cdr (assoc 0 p)) "ATTRIB")
					     (= (cdr (assoc 2 p)) "LIST_NUM")
					)
				))
						

				      (tg_get_following_data x))))))
		     (setq base_point (cdr (assoc 10 (entget x))))
		     (vlax-invoke (vlax-ename->vla-object x) 'getDynamicBlockProperties)
		  )
	))

		    (list (cons "LIST_NUM" list_num))
		    (list (cons "BASE_POINT" base_point))
		)))
(vl-remove-if-not (function (lambda (x)

 (and
   (= "INSERT" (cdr (assoc 0 (entget x))))
   (= block (vla-get-EffectiveName (vlax-ename->vla-object x))))
			      ))
   (mapcar 'cadr
     (vl-remove-if-not (function (lambda (x)
				(= (type (cadr x)) 'ename)
			))(ssnamex ss))))))

  
  (if  (null (setq scale (tg_get_scale))) (setq scale 1))
  (setq scale (/ 1 scale))

  (mapcar (function (lambda (x)
			(tg_new_layout
			  (cdr (assoc "LIST_NUM" x))
			  (tg_prug_center
			    (cdr (assoc "BASE_POINT" x))
			    (cdr (assoc "WIDTH" x))
			    (cdr (assoc "HEIGHT" x))
			    (cdr (assoc "ANG" x)))
			  (* scale (cdr (assoc "WIDTH" x)))
			  (* scale (cdr (assoc "HEIGHT" x)))
			  (* -1 (cdr (assoc "ANG" x)))
			  )
		      )) ss)
(comm "_regenall")
);end defun

(defun tg_change_block (new_block is_ss / blocks old_blocks_names tmp)

(tg_load_block_library (list new_block))
(setq old_blocks_names (mapcar 'cdr (tg_x-record_load "TG_onimport_entries" "TG_onimport_blocks")))
  
(if is_ss
  (setq blocks (tg_enames_from_ss_blocks old_blocks_names nil))
  (setq blocks (tg_enames_from_ss_blocks old_blocks_names (ssadd (car (entsel "�������� ����")))))
);end if

(if blocks

  (progn
    (setq params (tg_block_variables nil))
    (setq plan_ucs (TG_set_UCS ""))
    (setq snap (tg_snap nil))
    (mapcar (function (lambda (x / entfx position)
	        (setq position (cdr (assoc 10 (entget x)))
		      entfx (tg_get_following_data x)
		      atrib
		         (list
		          (list "OTM"
			    (if
			      (setq tmp
			       (cdr
			        (assoc 1
			         (car
			          (vl-remove-if-not (function (lambda (y)
						(= (cdr (assoc 2 y)) "OTM")
							)) entfx))))
			      );_setq
			      tmp
			      ""
			      );_if
		             )
		          (list "KOD"
			   (if
			    (setq tmp
			     (cdr
			      (assoc 1
			       (car
			        (vl-remove-if-not (function (lambda (y)
						(= (cdr (assoc 2 y)) "KOD")
							)) entfx)))))
			    tmp
			    ""
			    );_if
			   )
		          (list "NUM"
			   (if
			    (setq tmp
			     (cdr
			      (assoc 1
			       (car
			        (vl-remove-if-not (function (lambda (y)
						(= (cdr (assoc 2 y)) "NUM")
							)) entfx)))))
			    tmp
			    ""
			    );_if
			   )


		          (list "DATE"
			   (if
			    (setq tmp

			     (cdr
			      (assoc 1
			       (car
			        (vl-remove-if-not (function (lambda (y)
						(= (cdr (assoc 2 y)) "DATE")
							)) entfx)))))
			    tmp
			    ""
			    );_if
			  )
		      )
		      );_setq
		      (entdel x)
		       (tg_insert_block
			new_block
			position
			"������� ��"
			(tg_get_scale)
			atrib
			(caddar (vl-remove-if-not (function (lambda (y) (= new_block (cadr y))))  (car (tg_read_key_from_file keys))))
			
			)));_function_lambda
	    blocks
  );end mapcar
(tg_block_variables params)
(if plan_ucs (TG_set_UCS plan_ucs))
(tg_snap snap)
);end progn

);end if
  
);end defun

(defun tg_points_propol (blocks / )

;;;������� ��������� ��� ������, ������� �������� ���
;;;������� ������������ �� ���� <���� ������>_invis
;;;������� ������������:
;;;  			���������� ������ 25�*�������
;;;  			������� ������ 25��*�������
  
  (if (not blocks)
    (setq blocks
	   (tg_enames_from_ss_blocks
	     (mapcar 'cdr (tg_x-record_load "TG_onimport_entries" "TG_onimport_blocks"))
	     (ssadd (car (entsel "�������� ����"))))))

);end defun


(defun tg_change_atr_by_position (ucs is_conf / position cont)

(if (and (not ucs) (not is_conf))

(progn

  (initget 0 (strcat "<������� ��>" (tg_list_to_string_space (mapcar (function (lambda (x) (cdr (assoc 2 x)))) (TG_get_ucs)))))
  (setq ucs (getkword (strcat "������������� �� <������� ��> " "["
			   (vl-string-translate " " "/"
			     (vl-string-subst "" " "
			       (tg_list_to_string_space
			       (mapcar (function (lambda (x) (cdr (assoc 2 x))))
				       (TG_get_ucs)))
			       )
			     )
		    "] :"  )
		   ))


  )
  )
(setq cont t)
(while cont

(princ "\n �������� ���� ��� ������������")
(tg_try
  (function (lambda nil (setq block (car (entsel)))))
  '(setq cont nil)
);end try
(if (and
      cont
      block
      (= "INSERT" (cdr (assoc 0 (entget block))))
      (tg_get_following_data block)
     )

  (progn
    (setq position
	   (mapcar (function (lambda (x) (fix (+ x 0.5))))
	    (tg_trans_from_UCS_to_UCS (cdr (assoc 10 (entget block))) nil ucs)))
    (tg_atr_entmod block "X" (car position))
    (tg_atr_entmod block "Y" (cadr position))
    (tg_atr_entmod block "OTM" (caddr position))

  );_progn

  )
  
);end while

);end defun

