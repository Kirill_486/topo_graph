(defun tg_curent_file_path ( / )
       (getvar 'dwgprefix)
);end defun

(defun tg_write_organization_to_reg (val / )
(vl-registry-write "HKEY_CURRENT_USER\\Software\\TG_Group\\TOPO_GRAPH" "Organization" val)
);end defun

(defun tg_write_tg_path_to_reg (val / )
(vl-registry-write "HKEY_CURRENT_USER\\Software\\TG_Group\\TOPO_GRAPH" "Path" val)
);end defun

(defun tg_write_Path_keyconfig_to_reg (val / )
(vl-registry-write "HKEY_CURRENT_USER\\Software\\TG_Group\\TOPO_GRAPH" "Path_keyconfig" val)
);end defun

(defun tg_write_Path_block_lib_to_reg (val / )
(vl-registry-write "HKEY_CURRENT_USER\\Software\\TG_Group\\TOPO_GRAPH" "Path_Block_Library" val)
);end defun


(defun tg_write_name_to_reg (num val /)
(vl-registry-write "HKEY_CURRENT_USER\\Software\\TG_Group\\TOPO_GRAPH" (strcat "Name" (itoa num)) val)
);end defun

(defun tg_write_Job_to_reg (num val /)
(vl-registry-write "HKEY_CURRENT_USER\\Software\\TG_Group\\TOPO_GRAPH" (strcat "Job" (itoa num)) val)
);end defun


;
;----------------------------------------------------------------------------------------

(defun tg_read_organization_from_reg ( / )
(vl-registry-read "HKEY_CURRENT_USER\\Software\\TG_Group\\TOPO_GRAPH" "Organization")
);end defun

(defun tg_read_tg_path_from_reg ( / )
(vl-registry-read "HKEY_CURRENT_USER\\Software\\TG_Group\\TOPO_GRAPH" "Path")
);end defun

(defun tg_read_Path_keyconfig_from_reg ( / )
(vl-registry-read "HKEY_CURRENT_USER\\Software\\TG_Group\\TOPO_GRAPH" "Path_keyconfig")
);end defun

(defun tg_read_Path_block_lib_from_reg ( / )
(vl-registry-read "HKEY_CURRENT_USER\\Software\\TG_Group\\TOPO_GRAPH" "Path_Block_Library")
);end defun

(defun tg_read_Persons_from_reg ( / )
(mapcar (function (lambda (x)
		(list
		  (vl-registry-read "HKEY_CURRENT_USER\\Software\\TG_Group\\TOPO_GRAPH" (strcat "Name" (vl-princ-to-string x)))
		  (vl-registry-read "HKEY_CURRENT_USER\\Software\\TG_Group\\TOPO_GRAPH" (strcat "Job" (vl-princ-to-string x)))
		    )))'(1 2 3 4 5 6))
);end defun

;
;----------------------------------------------------------------------------------------

(defun tg_initialization ( / )

;;;��������� �������� ����� ����� ������������
;;;����� ���������� ������
;;;� �������� �������� �� ������� ��������� ��������� ����� (������ ���� ������ �������)

(if (or (not (findfile (tg_read_Path_keyconfig_from_reg)))
	(not (findfile (tg_read_Path_block_lib_from_reg))))
	(progn (alert "���������� ������� ������ ���� � ����� ���������� ������ � ����� ������������ ������-����������")
	(exit)
	);end progn
 );end if
  
(if (not (tg_get_scale)) (progn (alert "���������� ��������� ��������� �����") (exit)))
       
);end defun



(defun tg_plan_parameters_dialog ( / dialog_id plan_name org surv_type plan_scale persons x-record)


(if (setq x-record (tg_x-record_load "TG_plan_parameters" "TG_plan_parameters"))

  (progn
	(setq plan_name  (cdr (assoc 300 x-record)))
	(setq org        (cdr (assoc 301 x-record)))
	(setq surv_type  (cdr (assoc 71 x-record)))
	(setq plan_scale (cdr (assoc 41 x-record)))
	(setq persons    (mapcar 'cdr (tg_get_DXF_pairs (list 305 306) x-record)))
    	(if persons (setq persons    (mapcar (function (lambda (x) (list (nth x persons) (nth (1+ x) persons)))) '(0 2 4))))
  )

  (progn

    
    	(setq plan_name "")
    	(setq org        (tg_read_organization_from_reg ))
    	(setq persons (tg_read_Persons_from_reg))

  );end progn else


);end if

  
(if (< (setq dialog_id (load_dialog "tg_dialogs.dcl")) 0)
  	(progn (alert "�� ������� ����� ���� ������� tg_dialogs.dcl")(exit))
);end if

(if (not (new_dialog "tg_plan_parameters" dialog_id)) (progn (alert "������ tg_plan_parameters �� ����� ���� ������"))

(progn
  (if plan_name (set_tile "plan_name" plan_name))
  (if org (set_tile "organization" org))
  (if surv_type (set_tile "plan_type" (itoa surv_type)))

  (cond
    ((= plan_scale 0.5) (set_tile "plan_scale" "0"))
    ((= plan_scale 1) (set_tile "plan_scale" "1"))
    ((= plan_scale 2) (set_tile "plan_scale" "2"))
    ((= plan_scale 5) (set_tile "plan_scale" "3"))
  );end cond

  (if persons (mapcar (function (lambda (x / tile)
				  (if (setq tile (car (nth x persons))) (set_tile (strcat "percon_name" (vl-princ-to-string x)) tile ))
				  (if (setq tile (cadr (nth x persons))) (set_tile (strcat "percon_job" (vl-princ-to-string x)) tile ))
				  )) '(0 1 2 3 4 5)))
);end progn
  

);end if

(action_tile "accept"    "(setq x-record (list (cons 300 (get_tile \"plan_name\"))
					 (cons 301 (get_tile \"organization\"))
					 (cons 71  (atoi (get_tile \"plan_type\")))
					 (cons 41  (cond ((= (get_tile \"plan_scale\") \"0\") 0.5 )
							 ((= (get_tile \"plan_scale\") \"1\") 1 )
							 ((= (get_tile \"plan_scale\") \"2\") 2 )
							 ((= (get_tile \"plan_scale\") \"3\") 5 )
							 ))));end cond cons
					
			  (mapcar (function (lambda (x / name job)
					    

					(if (and (/= (setq name (get_tile (strcat \"percon_name\" (vl-princ-to-string x)))) \"\" )
						 (/= (setq job  (get_tile (strcat \"percon_job\" (vl-princ-to-string x)))) \"\"))

					    (setq x-record (append x-record (list(cons 305 name))
									    (list(cons 306 job))
									  ))

					)

			  		    ))
			  		 '(0 1 2 3 4 5))
		  	  (done_dialog 1)"  
  );end action_tile

(if (= (start_dialog) 1) (tg_x-record_save "TG_plan_parameters" "TG_plan_parameters" x-record))
(unload_dialog dialog_id)
);end defun



(defun tg_read_30_strings (inp_file / str res)
;;;
;;;��������� ������ 30 ����� �� ����� 
;;;� ������ �� � ���� ������ �����
;;;
(setq inp_file (open inp_file "r"))
(repeat 100
  (setq str (read-line inp_file))
  (setq res (append res (list str)))  
);end while
(close inp_file)
(vl-remove-if 'null res)
);end defun




(defun tg_default_parameters_dialog ( / persons org keyconf dialog_id tmp b_lib)

(if (< (setq dialog_id (load_dialog "tg_dialogs.dcl")) 0)
  	(progn (alert "�� ������� ����� ���� ������� tg_dialogs.dcl")(exit)))

(if (not (new_dialog "tg_default_parameters" dialog_id))
  (progn (alert "������ tg_default_parameters �� ����� ���� ������") (exit)) 

(progn  

  (if (setq org     (tg_read_organization_from_reg)) (set_tile "organization" org))
  (if (setq persons (tg_read_Persons_from_reg)) (mapcar (function (lambda (x / tile)
				  (if (setq tile (car (nth x persons))) (set_tile (strcat "percon_name" (vl-princ-to-string x)) tile ))
				  (if (setq tile (cadr (nth x persons))) (set_tile (strcat "percon_job" (vl-princ-to-string x)) tile ))
				  )) '(0 1 2 3 4 5)))
  (if (/= (setq keyconf (tg_read_Path_keyconfig_from_reg)) "") (set_tile "path" keyconf )
    (if (/= (setq keyconf (tg_read_tg_path_from_reg)) "") (set_tile "path" (setq keyconf (strcat keyconf "\\")))))


  (if (/= (setq b_lib (tg_read_Path_block_lib_from_reg)) "") (set_tile "block_lib" b_lib )
    (if (/= (setq b_lib (tg_read_tg_path_from_reg)) "") (set_tile "block_lib" (setq b_lib (strcat b_lib "\\")))))
  
  (action_tile "search_bt" "(if (setq tmp (getfiled \"������� ���� ������-����������\" keyconf \"cfg\" 0)) (progn (set_tile \"path\"  tmp) (setq keyconf tmp)))")
  (action_tile "search_bl" "(if (setq tmp (getfiled \"������� ���� ���������� ������\" b_lib \"dwg\" 0)) (progn (set_tile \"block_lib\"  tmp) (setq b_lib tmp)))")
  (action_tile "accept" (strcat
			"(tg_write_organization_to_reg (get_tile \"organization\"))"
			"(tg_write_Path_keyconfig_to_reg (get_tile \"path\"))"
			"(tg_write_Path_block_lib_to_reg (get_tile \"block_lib\"))"
			"(mapcar (function (lambda (x)
						(tg_write_name_to_reg x (get_tile (strcat \"percon_name\" (itoa (1- x)))))
					     	(tg_write_Job_to_reg x  (get_tile (strcat \"percon_job\" (itoa (1- x)))))
					     )) '(1 2 3 4 5 6))"
			"(done_dialog 1)"
			); 

  );end at



);end progn


);end if

(start_dialog)
(unload_dialog dialog_id)
);end defun

(defun tg_list_refresh (el_key inp_list /)
;;;������� ������������ ���� � ����� el_key
;;;������� ��� � ������������ �� ������� inp_list
(start_list el_key 3)
(mapcar 'add_list inp_list)
(end_list)
);end defun

(defun tg_import_tgkrd_file_dialog ( / dialog_id ucs keys files onimport res)
;;;
;;;������ ����������� ����� ��� �������, ������� ���������, ��� ��
;;;����� ������.
;;;
(if (< (setq dialog_id (load_dialog "tg_dialogs.dcl")) 0)
  	(progn (alert "�� ������� ����� ���� ������� tg_dialogs.dcl")(exit)))
(if (not (new_dialog "tg_import_krd_file" dialog_id))
  (progn (alert "������ tg_import_krd_file �� ����� ���� ������") (exit)))

(if (not (tg_get_scale)) (progn (alert "����� �������� ������ ������� ���������� ����������������") (exit)))  
(setq ucs (mapcar (function (lambda (x) (tg_get_DXF_pairs '(2 15) x))) (tg_get_ucs)))
(setq ucs (append '(((2 . "������� ��")(15 . 1)))  ucs))
(start_list "SK" 3)
(mapcar (function (lambda (x) (add_list (cdr (assoc 2 x))))) ucs)
(end_list)
  
(start_list "SK_type" 3)
(mapcar 'add_list '("Geod" "World"))
(end_list)
(set_tile "SK_type" (itoa (cdr (assoc 15 (nth 0 ucs)))))
(action_tile "SK" "(set_tile \"SK_type\" (itoa (cdr (assoc 15 (nth (atoi (get_tile \"SK\")) ucs)))))")  

(setq keys (mapcar (function (lambda (x) (cadar x)))(tg_read_key_from_file nil)))
(start_list "keyconfig" 3)
(mapcar 'add_list keys)
(end_list)

(action_tile "file_list" "(tg_list_refresh \"preview\" (tg_read_30_strings (nth (atoi(get_tile \"file_list\")) files)))")  
(action_tile "plus" "(if (not (member (setq tmp (getfiled \"������� �������� ���� ���������\" (tg_curent_file_path) \"tgkrd;krd;prn;txt;*\" 4)) files)) (progn (setq files (append files (list tmp))) (tg_list_refresh \"file_list\" files) ) (alert \"���� ���� ��� ��������\"))")
(action_tile "minus" "(if (setq num (TG_str_to_list_num (get_tile \"file_list\"))) (progn (setq files (TG_list_remove-i (car num) files)) (tg_list_refresh \"file_list\" files) (tg_list_refresh \"preview\" '())) (alert \"��� �������� ����� ������� ����\") )")
(action_tile "clear" "(start_list \"file_list\" 3) (end_list) (setq files nil) (tg_list_refresh \"preview\" '())")

(action_tile "accept" "(cond 	((null files) (alert \"����� ������ ���� �� ���� ���� ��� �������\"))
				((null (setq onimport (car (tg_str_to_list (get_tile \"onimport\"))))) (alert \"���������� ������ ��� ������� �������\"))
				((member onimport (mapcar (function (lambda (x) (cdr x) )) (TG_X-record_list \"TG_onimport\") ))
						(alert \"����� ������� ������� ��� ����������. ���������� ������ ���.\"))
				(T (progn
					(setq res (list files
							(cdar (nth (atoi(get_tile \"SK\")) ucs))
							(nth (atoi(get_tile \"keyconfig\")) keys)
							(get_tile \"onimport\")
							(atoi (get_tile \"SK_type\"))
							)
						
					)
					(done_dialog 1)))
				)"
  )
  
(start_dialog)
(unload_dialog dialog_id)
(mapcar (function (lambda (x) (tg_import_points_from_file x (nth 1 res)(nth 2 res)(nth 3 res) (nth 4 res)) ))(car res))
);end defun


(defun tg_krd_compliment_dialog ( / dialog_id tmp files t1 t2 t3)

(if (< (setq dialog_id (load_dialog "tg_dialogs.dcl")) 0)
  	(progn (alert "�� ������� ����� ���� ������� tg_dialogs.dcl")(exit)))
(if (not (new_dialog "tg_compliment_krd" dialog_id))
  (progn (alert "������ tg_compliment_krd �� ����� ���� ������") (exit)))

(action_tile "search_krd" "(if (setq tmp (getfiled \"������� �������� ���� ���������\" (tg_curent_file_path) \"krd;prn;txt;*\" 4)) (set_tile \"krd_file\"  tmp))")
(action_tile "search_sdr" "(if (setq tmp (getfiled \"������� �������� ���� ����������\" (tg_curent_file_path) \"sdr\" 0)) (set_tile \"sdr_file\"  tmp))")
(action_tile "search_tgkrd" "(if (setq tmp (getfiled \"������� �������� ���� ���������\" (tg_curent_file_path) \"tgkrd\" 1)) (set_tile \"tgkrd_file\"  tmp))")  
(action_tile "accept" "(if (and
				(/= (setq t3 (get_tile \"tgkrd_file\")) \"\")
				(/= (setq t2 (get_tile \"sdr_file\"  )) \"\")
				(/= (setq t1 (get_tile \"krd_file\"  )) \"\"))

				(progn (setq files (list t1 t2 t3 (get_tile \"date\"))) (done_dialog 1))
				(alert \" ��� ����������� ���������� ������� �������� � �������������� �����\"))"
			
  );end at
(start_dialog)
(unload_dialog dialog_id)
(tg_save_list_to_file (caddr files)
  (tg_compliment_krd_with_field_data (tg_read_file_list_strings (car files))
    (tg_read_sdr_information (cadr files)) (cadddr files)))  
);end defun


(defun tg_search_points_dialog ( / dialog_id x-blocks x-kodes x-onimport tmp num out_file out_ucs ucs ss dd ucs_type kd_all)

(if (< (setq dialog_id (load_dialog "tg_dialogs.dcl")) 0)
  	(progn (alert "�� ������� ����� ���� ������� tg_dialogs.dcl")(exit)))
(if (not (new_dialog "tg_search_points" dialog_id))
  (progn (alert "������ tg_search_points �� ����� ���� ������") (exit)))

(tg_list_refresh "blocks_list" (setq x-blocks
			      (acad_strlsort (mapcar 'cdr (tg_x-record_load "TG_onimport_entries" "TG_onimport_blocks"))
				     )))
(tg_list_refresh "kodes_list" (setq x-kodes
			      (acad_strlsort (mapcar 'cdr (tg_x-record_load "TG_onimport_entries" "TG_onimport_kodes"))
				    )))
(tg_list_refresh "onimport_list" (setq x-onimport
				(acad_strlsort (mapcar 'cdr (tg_get_DXF_pairs (list 3) (TG_X-record_list "TG_onimport")))
					)))

(tg_list_refresh "SK" (setq ucs (append '("������� ��")  (mapcar (function (lambda (x) (cdr (assoc 2 x)))) (tg_get_ucs) ))))

(if (or (null x-blocks)(null x-kodes)(null x-onimport))
  (progn (alert "��� ������ �����, � ����� ������ ������������ ���� �� ���� ������� �������") (exit) )
);end if

(mode_tile "res_file" 1) (mode_tile "search_res" 1) (mode_tile "SK" 1)

(action_tile "method" "(if (= (get_tile \"method\") \"select\")
		(progn (mode_tile \"res_file\" 1) (mode_tile \"search_res\" 1) (mode_tile \"SK\" 1))
		(progn (mode_tile \"res_file\" 0) (mode_tile \"search_res\" 0) (mode_tile \"SK\" 0)))")
(action_tile "search_res" "(if (setq tmp (getfiled \"������� �������� ���� ���������\" (tg_curent_file_path) \"txt\" 1))
				(set_tile \"res_file\"  tmp) )")


(action_tile "onimport_all" "(setq num -1 tmp \"\") (set_tile \"onimport_list\"
  			(repeat (length x-onimport)
  				(setq tmp (strcat tmp \" \" (vl-princ-to-string (setq num (1+ num)))))
  			))")
(action_tile "onimport_clear" "(set_tile \"onimport_list\" \"\")")
(action_tile "blocks_all" "(setq num -1 tmp \"\") (set_tile \"blocks_list\"
  			(repeat (length x-blocks)
  				(setq tmp (strcat tmp \" \" (vl-princ-to-string (setq num (1+ num)))))
  			))")
(action_tile "blocks_clear" "(set_tile \"blocks_list\" \"\")")

(action_tile "kodes_all" "(if (=  (get_tile \"kodes_all\") \"1\")

				(progn
					   (mode_tile \"kodes_list\" 1)
					   (mode_tile \"kodes_clear\" 1)
					   (setq kd_all T)
				)
				(progn
					   (mode_tile \"kodes_list\" 0)
					   (mode_tile \"kodes_clear\" 0)
					   (setq kd_all nil)
				)
			)"

  )  
(action_tile "kodes_clear" "(set_tile \"kodes_list\" \"\")")

  
(action_tile "accept" 	"(setq x-onimport (mapcar (function (lambda (x) (nth x x-onimport)) ) (TG_str_to_list_num (get_tile \"onimport_list\"))) )
			 (setq x-blocks   (mapcar (function (lambda (x) (nth x x-blocks)) )   (TG_str_to_list_num (get_tile \"blocks_list\"))) )
			 (setq x-kodes    (mapcar (function (lambda (x) (nth x x-kodes)) )    (TG_str_to_list_num (get_tile \"kodes_list\"))) )
			 (if (= (get_tile \"method\") \"to_file\")
			 	(progn
			 		(setq out_file (get_tile \"res_file\"))
			 		(setq out_ucs (nth (atoi (get_tile \"SK\")) ucs))
			 	))

			 (if (and x-onimport x-blocks (or x-kodes kd_all))
			   (if (and (= (get_tile \"method\") \"to_file\") (= (get_tile \"res_file\") \"\"))(alert \"������� ���������� ���� ����������\") (done_dialog 1))			 
			 (alert \"������� ������� �������, ����� � ���� ����� ��� ������\")
			 )
			")
  

(setq dd (start_dialog))
(unload_dialog dialog_id)

(if (and (= dd 1) (not out_file))
 (progn
   (setq ss (ssadd))
   (mapcar (function (lambda (x) (ssadd (cdr (assoc -1 (car x))) ss))) (tg_seatch_blocks x-onimport x-blocks x-kodes kd_all))
   (sssetfirst nil ss)
 )
)

(if (and (= dd 1) out_file)
(progn
(setq ucs_type
       (car
	 (vl-remove-if 'null
	   (mapcar (function (lambda (x) (if (= (cdr (assoc 2 x)) out_ucs ) (cdr(assoc 15 x)) )))
	   (tg_get_ucs))
	 )
))
(setq ss (mapcar (function (lambda (x / tmp) (list

					(if (setq tmp (car (vl-remove-if 'null
						   (mapcar (function (lambda (y)
								(if (and (= (cdr (assoc 0 y)) "ATTRIB")(= (cdr(assoc 2 y)) "NUM"))
								  (cdr (assoc 1 y))))) x))))
						tmp
					  	"no_num";else
					 );end if NUM

					(progn
					 (setq tmp (mapcar (function (lambda (y) (rtos y 2 2)))
							(if (setq tmp (tg_trans_to_ucs (cdr (assoc 10 (car x))) out_ucs))
						  	tmp
						  	(cdr (assoc 10 (car x))))))
					 (if (= ucs_type 0) (strcat (nth 0 tmp) " " (nth 1 tmp)) (strcat (nth 1 tmp) " " (nth 0 tmp)))
					);position

					(if (setq tmp(car (vl-remove-if 'null
						   (mapcar (function (lambda (y)
								(if (and (= (cdr (assoc 0 y)) "ATTRIB")(= (cdr(assoc 2 y)) "OTM"))
								  (cdr (assoc 1 y))))) x))))
					  	tmp
					 	"no_otm";else
					 );end if OTM

					 (if (setq tmp (car (vl-remove-if 'null
					   (mapcar (function (lambda (z) (if (= (vla-get-propertyname z) "VIS") (vla-get-value z))
							     ))
						 (vlax-invoke (vlax-ename->vla-object (cdr (assoc -1 (car x)))) 'GetDynamicBlockProperties )))))
					 (vlax-variant-value tmp)
					 "no_vis"
					   )
					
					 ) )) (tg_seatch_blocks x-onimport x-blocks x-kodes kd_all) ));end setq


(tg_save_list_to_file out_file (mapcar 'tg_list_to_string_space ss))

  )
)
  
);end defun


(defun tg_refresh_onimport_points_dialog ( / dialog_id
					     unknown_bl
					     dialog_res
					     kodes_sel
					     kodes_unsel
					     blks
					     contin
					     onimport_names
					     onimport_name
					     onimport_data
					     dialog_num
					  )

(tg_refresh_x-record "tg_Onimport" t)

(setq dialog_res 1000)
(setq onimport_names (mapcar 'cdr (vl-remove-if-not (function (lambda (x) (= (car x) 3))) (TG_X-record_list "TG_Onimport"))))
(setq unknown_bl (mapcar (function (lambda (x / z)

				  (if (not (member
					     (if (null (setq z (tg_get_attr "KOD" x))) (setq z "nil") z)
					     kodes_unsel))
					(setq kodes_unsel (append kodes_unsel (list z)))
				    )
				  (list (cons nil x) (cons z (cdr (assoc 5 (entget x)))))
				     ))


		   (tg_find_unknown_points)))

(if (null unknown_bl) (progn (alert "�� ������� ����������������� ������ �� ������������� ������� �������") (exit)))



(while (and (<= 1 dialog_res) (null contin))

(if (< (setq dialog_id (load_dialog "tg_dialogs.dcl")) 0)
  	(progn (alert "�� ������� ����� ���� ������� tg_dialogs.dcl")(exit)))
(if (not (new_dialog "tg_refresh_points" dialog_id))
  (progn (alert "������ tg_refresh_points �� ����� ���� ������") (exit)))
(action_tile "move_to"   (strcat
			   	"(setq blks (mapcar (function (lambda (x)(nth x kodes_unsel)))"
				"(TG_str_to_list_num (get_tile \"kodes_list\"))))"
				"(done_dialog 2)"
				))

(action_tile "move_out" (strcat
			  	"(setq blks (mapcar (function (lambda (x)(nth x kodes_sel)))"
			  	"(TG_str_to_list_num (get_tile \"kodes_sel_list\"))))"
				"(done_dialog 4)"
				))

(action_tile "move_all_to" "(done_dialog 3)")
(action_tile "move_all_out" "(done_dialog 5)")
(action_tile "accept" (strcat
			"(if 	(and (/= \"\" (setq onimport_name (get_tile \"onimport\")))"
					"(not (member onimport_name onimport_names ))  )"
					"(done_dialog 1)"
					"(alert \"��� ������� ������� ������ ���� �������� ����������\"))"
			))


(tg_list_refresh "kodes_sel_list" (setq kodes_sel (acad_strlsort kodes_sel)))  
(tg_list_refresh "kodes_list" (setq kodes_unsel (acad_strlsort kodes_unsel)))
  
(setq dialog_res (start_dialog))  

(cond
((= dialog_res 1) (progn
                (TG_X-record_save "TG_Onimport" onimport_name
                (mapcar (function (lambda (p) (cons 300 p)))
                  (mapcar (function (lambda (x)
                            (car (vl-remove-if 'null (mapcar (function (lambda (y / z)
                                (if (setq z (assoc y x)) (cdr z))
                                    )) kodes_sel)))
                            )) unknown_bl )))
            (if (null kodes_unsel)(setq contin t))
            ))

((= dialog_res 2) (progn (setq kodes_sel (append kodes_sel blks))
			       (mapcar (function (lambda (x)
					(setq kodes_unsel (vl-remove x kodes_unsel))
			       ))  blks )))
((= dialog_res 4) (progn (setq kodes_unsel (append kodes_unsel blks))
			       (mapcar (function (lambda (x)
					(setq kodes_sel (vl-remove x kodes_sel))
			       ))  blks )))
((= dialog_res 3) (setq kodes_sel (append kodes_sel kodes_unsel) kodes_unsel nil))
((= dialog_res 5) (setq kodes_unsel (append kodes_unsel kodes_sel) kodes_sel nil))
);end cond
(unload_dialog dialog_id)
  
);end while

);end defun

;;;(defun tg_import_points_from_pline (/ dialog_id dialog_res pp_kod pp_onimport)
;;;
;;;
;;;(if (< (setq dialog_id (load_dialog "tg_dialogs.dcl")) 0)
;;;  	(progn (alert "�� ������� ����� ���� ������� tg_dialogs.dcl")(exit)))
;;;(if (not (new_dialog "tg_import_points_from_pline" dialog_id))
;;;  (progn (alert "������ tg_import_points_from_pline �� ����� ���� ������") (exit)))
;;;
;;;(action_tile "search_obj" "	(setq pp_kod (get_tile \"kod\"))
;;;				(setq pp_onimport (get_tile \"onimport\"))
;;;				(done_dialog 5)")
;;;
;;;  
;;;(start_dialog)
;;;(unload_dialog dialog_id)
;;;
;;;);end deun


(defun tg_get_new_value_dialog (/ dialog_id value)

(if (< (setq dialog_id (load_dialog "tg_dialogs.dcl")) 0)
  	(progn (alert "�� ������� ����� ���� ������� tg_dialogs.dcl")(exit)))
(if (not (new_dialog "tg_one_attr_change_dialog" dialog_id))
  (progn (alert "������ tg_compliment_krd �� ����� ���� ������") (exit)))
(action_tile "accept" "(setq value (get_tile \"attr\")) (done_dialog 1)")
(start_dialog)  
(unload_dialog dialog_id)
value
);end defun


