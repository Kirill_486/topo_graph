(defun tg_cui_detect ( / cui_list Mgroups tmp i)
(vl-load-com)
(setq Mgroups (vla-get-MenuGroups (vlax-get-acad-object)))

(setq i 0)
(while
  (not (vl-catch-all-error-p (vl-catch-all-apply (function
						   (lambda (x) (setq tmp (vla-item Mgroups x)))
						 ) (list i))))
  (setq
    cui_list (append cui_list (list (vla-get-Name tmp)))
    i (1+ i)
  )
  );_while
cui_list
);end degun


(defun tg_load_cui (/ cui_list cui_list_to_load Mgroups)

(setq cui_list_to_load '(
			"Topo_Graph_030"
;���� ���������� �� ���������, ������� ���������� �������
			 ))
(setq cui_list (tg_cui_detect))
(setq cui_list_to_load (vl-remove-if
			 (function
			   (lambda (x) (member x cui_list))
			  ) cui_list_to_load))
(setq Mgroups (vla-get-MenuGroups (vlax-get-acad-object)))

(mapcar
  (function
    (lambda (cui_file)
     (if 
      (vl-catch-all-error-p
	(vl-catch-all-apply
	  (function (lambda (y)
		      (vla-load Mgroups y))) (list cui_file))

     )
    (alert (strcat "��������� ������� �������� ���������" cui_file))
    )
   )
  )
 cui_list_to_load)

);end defun

(defun tg_try (protected_exp on_error_exp / result)
;;;������� ��������� ��������� protected_exp
;;;��� ������������� ������ (������� Esc ��������),
;;;��������� on_error_exp
;;;���� ��������� ����������� ��� ������, ��������� then_exp
  (setq result (vl-catch-all-apply protected_exp))
  (if (vl-catch-all-error-p result) (eval on_error_exp) result)
);_defun


(defun TG_Get_Poly_Points (obj / res obj_part)
;������ ������ ��������� �����-������

  (setq rec (lambda (name / n_a)
    (progn (setq n_a (entget name))  
       (cond
	 ( (= (cdr(assoc 0 n_a)) "VERTEX")
	   (append (list (assoc 10 n_a)) (apply (function rec) (list (entnext name))))
	); end vertex
          ( (= (cdr(assoc 0 n_a)) "SEQEND") '(nil)); end seqend
        ); end cond
     ); end progn
    );end lambda
  ); end setq
(setq obj_part (entget obj))
(cond
   ((= (cdr(assoc 0 obj_part)) "LWPOLYLINE")); end lwpoly
   ((= (cdr(assoc 0 obj_part)) "LINE") (setq obj_part (append (list(assoc 10 obj_part)) (list(assoc 11 obj_part)))))
   ((= (cdr(assoc 0 obj_part)) "POLYLINE")   
    (setq obj_part (apply (function rec) (list (entnext obj))))     
   ); end poly
     (t "");end t
); end cond
     (setq obj_part (mapcar 'cdr (vl-remove-if-not '(lambda (x) (or (= (car x) 10) (= (car x) 11))) obj_part)))
     (mapcar (function (lambda (x) (trans x 1 0))) obj_part)
); end defun



(defun tg_list_sort_by_function (func inp_list / op_list tpp out_list)
;;;
;;;��������� ������ �� �������� ��������, ������ ������� �� ������
;;;����������� � ������� ������� ������� func.
;;;������� ������������� ��������.

(mapcar
  (function (lambda (x)
		(foreach j inp_list

		    (if (and (not (member j out_list)) (= (vl-princ-to-string (apply func (list j))) x)) (setq out_list (append out_list (list j))) )


		  )
		    ))
  (acad_strlsort (mapcar 'vl-princ-to-string (mapcar func inp_list)))
 )
(vl-remove-if 'null out_list)
  
);end defun



(defun tg_enames_from_ss_blocks (block ss /)

;;;��������� emane� ���� ��������� ������ � ������� �� ������ Block
;;;�� ������ ss. ���� ����� nil - ���� ����������� �������
  
  (if (not ss) (setq ss (ssget)))

  (vl-remove-if-not (function (lambda (x)

 (and
   (= "INSERT" (cdr (assoc 0 (entget x))))
   (member (vla-get-EffectiveName (vlax-ename->vla-object x))  block))
			      ))
   (mapcar 'cadr
     (vl-remove-if-not (function (lambda (x)
				(= (type (cadr x)) 'ename)
			))(ssnamex ss))))

);end defun



(defun tg_snap (snap / old_snap)

(if snap 	(setvar 'OSMODE snap)
		(progn

			(if (>= (setq old_snap (getvar 'OSMODE)) 16384)
			  old_snap
			  (progn (setvar 'OSMODE (+ old_snap 16384)) old_snap) )

		  )

  );end if  
  
);end defun

(defun tg_plan_coordinates (point /)

(list (nth 0 point) (nth 1 point) 0)
  
);end defun


(defun div (x y)
(fix (/ x y))  
);end defun

(defun mod (x y)
(- x (* (div x y) y))  
);end defun

(defun tg_list_to_string (inp_list / res)
;;;
;;;������� ����������� ������ � ������ �� ���������
;;;
(setq res "")
(mapcar (function (lambda (x) (setq res (strcat res "\n" (vl-princ-to-string x))))) inp_list )  
res
);end defun


(defun tg_list_to_string_space (inp_list / res)
;;;
;;;������� ����������� ������ � ������ �� ���������
;;;
(setq res "")
(mapcar (function (lambda (x) (setq res (strcat res " " (vl-princ-to-string x))))) inp_list )  
res
);end defun




(defun tg_set_layer (n_layer /)
;;;������������� ���� n_layer �������
(if (not (tblsearch "layer" n_layer))
  	(entmakex (list
		(cons 0 "LAYER")
		(cons 2 n_layer)
		    )) )
(vl-cmdf "_.LAYER" "_m" n_layer)
(vl-cmdf)
);end defun


(defun tg_get_DXF_pairs (pairs inp_list / )

;;;��������� ��������� �������� ���� ������ � ������
;;;������� ������ � ������ pairs
;;;
(vl-remove-if-not (function
		    (lambda (x) (member (car x) pairs))
	  )
	inp_list);end mapcar  
);end defun


(defun TG_list_remove-i (i lst)
;;;
;;;��������� ������� �� ������ ������� � ������� i
;;;
  (setq i (1+ i))
     (vl-remove-if '(lambda (x) (zerop (setq i (1- i)))) lst)
);end defun

(defun TG_member_match (match inp_list / )
;;;
;;;������� � ������ ������ ������� ������� ������������� �������
;;;match  ��� ������� wcmatch
(car (vl-remove-if-not (function (lambda (x) (wcmatch (vl-princ-to-string x) match))) inp_list)  )

);end defun

(defun tg_str_to_list (i_str /)
;;;������ � ������ �����
;;;������� ����������� ������ � ������������ ������
;;;� ������ �� �����
;;;i_str ������ �� �����
(if (= (type i_str) 'str)
(progn  
(setq i_str (read (strcat "(" i_str ")")))
(mapcar (function (lambda (x) (if (not (numberp x)) (vl-princ-to-string x) 

		    (progn
		      (if (= (type x) 'int) (itoa x) (rtos x 2 2))
		    );else
		    )))

	i_str);end mapcar
);end progn
nil ;else
  );end if  

);end defun



(defun TG_str_to_list_num (i_str /)
;;;������ � ������ �����
;;;������� ����������� ������ � ������������ ������
;;;� ������ ��������
;;;i_str ������ �� �����
(if (= (type i_str) 'str)
(progn  
(setq i_str (read (strcat "(" i_str ")")))
(mapcar (function (lambda (x) (if (not (numberp x)) (vl-princ-to-string x) x)))

	i_str);end mapcar
);end progn
nil ;else
  );end if  
); end defun


(defun tg_entmod (entry dxf_kode new_val / old_pair)
;;;
;;;������ � ��������� �������� DXF ���� �� �����
;;;��� ������� ���� � ����� �����
(if (setq old_pair (assoc dxf_kode (setq entry (entget entry))))
(entmod (subst (cons dxf_kode new_val) old_pair entry))
);end if  
);end defun

(defun tg_ActiveX_start (/)

(vl-load-com)
(setq curent_doc (vla-get-ActiveDocument (vlax-get-acad-object)))
  
);end defun


(defun tg_get_following_data (entry / result)
;;;��������� ������ ��������� �� ���������� ������
;;;(������� � ��������)
;;;  
(setq entry (entget entry))
(if (= (cdr (assoc 66 entry )) 1)

  (while (/= (cdr (assoc 0 (setq entry (entget (entnext (cdr (assoc -1 entry))))))) "SEQEND")
	(setq result (cons entry result ))

  );end while
  );end if

result  

);end defun

(defun tg_get_attr (attr entry / )

(cdr (assoc 1

    (car (vl-remove-if-not (function (lambda (x)

	(and
	  (= (cdr (assoc 2 x)) attr)
	  (= (cdr (assoc 0 x)) "ATTRIB")
	)

		    ))


  (tg_get_following_data entry)))))
);end defun


(defun tg_atr_entmod (entry atrib value /)
;;;
;;;��������� �������� �������� �������� atrib �����
;;;�� �������� value
(mapcar (function (lambda (x) (if (and (= (cdr (assoc 0 x)) "ATTRIB")
				       (= (cdr (assoc 2 x))  atrib))
		    (tg_entmod (cdr (assoc -1 x)) 1 (vl-princ-to-string value))))) (tg_get_following_data entry))

(princ)  
);end defun


(defun tg_block_variables (old_var / var)

(if (= old_var nil)
	(progn
  		(setq var (list (getvar 'ATTDIA) (getvar 'ATTREQ)))
	  	(setvar 'ATTDIA 0)
	  	(setvar 'ATTREQ 0)
	);end progn
  	(progn
	(setvar 'ATTDIA (car old_var))
  	(setvar 'ATTREQ (cadr old_var)));end progn

	

  );end if
var
);end defun



(defun tg_insert_block (name position ucs scale atribs bl_layer /  entry )
;;;�������� �������� ������� ������ ��� �������
;;;name		-��� �����
;;;position	-���������� �������
;;;ucs		-������� ���������
;;;scale	-������� �������
;;;atribs	-������ ���� (((�������)(��������))((�������)(��������)))
;;;bl_layer	-��� ���� � ������� ������������� ����

(setq position (tg_trans_from_ucs position ucs))


	(tg_set_layer bl_layer)  

;  	(setq params (tg_block_variables nil))
;  	(setq old_ucs (TG_set_UCS "������� ��"))
;	;����� ���� � ������� �������, ���� ������ �� � ���������� ��� ������ �������

(vl-cmdf "._-INSERT" name position scale 0)
(setq entry (entlast))
(mapcar (function (lambda (x) (tg_atr_entmod entry (car x) (cadr x)))) atribs )  


;	(tg_block_variables params)
;  	(if old_ucs (TG_set_UCS old_ucs))
;	;����� ���� � ������� �������, ���� ������ �� � ���������� ��� ������ �������


entry
  );end defun

(defun tg_alert (mess /)
;;;
;;;������� ������� alert
;;;��� ����������� ��������� �� ������ ���� � ������
  (alert (vl-princ-to-string mess))
);end defun


(defun SetDynamicBlockPropertyNameValue ( block prop value )

;;  Modifies the value of a Dynamic Block Property            ;;
;;------------------------------------------------------------;;
;;  Author: Lee Mac, Copyright � 2010 - www.lee-mac.com       ;;
;;------------------------------------------------------------;;
;;  Arguments:                                                ;;
;;  block - VLA Dynamic Block Reference Object                ;;
;;  prop  - Dynamic Block Property Name                       ;;
;;  value - New value for Property                            ;;
;;------------------------------------------------------------;;
;;  Returns: Value property was set to, else nil              ;;
;;------------------------------------------------------------;;
  
  ;; � Lee Mac 2010
  (vl-some
    (function
      (lambda ( _prop )
        (if (eq (strcase prop)(strcase (vla-get-propertyname _prop)))
          (progn
            (vla-put-value _prop
              (vlax-make-variant value
                (vlax-variant-type (vla-get-value _prop))
              )
            )
            value
          )
        )
      )
    )
    (vlax-invoke block 'GetDynamicBlockProperties)
  )
)
;;;�-��� ��������� ������� � �������
;;;( dtr a)
(defun DTR (a)(* pi (/ a 180.0)))

;;;�-��� ��������� ������� � �������
;;;( R2D a)
(defun RTD (a)(/ (* a 180.0) pi))


(defun TG_min_kratnoe (num delt /)

(* delt (fix (/ num delt)))  

);end defun

(defun tg_prug_center (base_point wi hi ang / )

  
  (mapcar (function (lambda (x y) (+ x y)))

		      (tg_plan_coordinates base_point)
		      (list
    			(/ (+ (* wi (cos ang)) (* hi (cos (+ ang (/ pi 2))))) 2)
    			(/ (+ (* wi (sin ang)) (* hi (sin (+ ang (/ pi 2))))) 2)
		      ) )
	
  
);end defun

(defun tg_debug (/)
(setq tg_is_debug (not tg_is_debug))
);end defun

(defun tg_debug_msg (var num / )
 (if tg_is_debug
   (progn
     (princ "\n �������.\n")
     (princ "����� ��   ")  (princ num)               (princ "\n")  
     (princ "���������� ")  (princ var)               (princ "\n")
     (princ "��������   ")  (princ (eval var))        (princ "\n") 
     (princ "���        ")  (princ (type (eval var))) (princ "\n")
     (getstring "\n ������ ��� �����������")

   )
 )
);_defun

(defun tg_3d_poly_interpolate (poly / verts vertsn verts0 n v_num line_len)

  (setq n -1
	v_num (cdr (assoc 70 (entget poly)))
	verts (mapcar (function (lambda (x) (list (setq n (1+ n)) (cdr (assoc -1 x)) (cdr (assoc 10 x)))))  (tg_get_following_data poly))
	verts0 (vl-remove-if-not (function (lambda (x) (= 0 (caddr (nth 2 x))))) verts)
	vertsn (vl-remove-if     (function (lambda (x) (= 0 (caddr (nth 2 x))))) verts)
	n 0
	)

  (repeat v_num

(setq line_len (append line_len (distance (tg_plan_coordinates ))))


  );_repeat


  
(princ)
);_defun