(defun TG_X-record_save ( dict el_name elem / root_dict c_dict)

  ;;;��������� � ������� � ������ dict
  ;;;������� � ������ el_name � ������� elem
  ;;;�������� ������� ��� ������� ������������
  ;;;


  
(setq root_dict (namedobjdict))

  (if ( not (setq c_dict (cdr (assoc -1 (dictsearch root_dict dict)))))
 	(setq c_dict (dictadd root_dict dict (entmakex '((0 . "DICTIONARY")(100 . "AcDbDictionary")))))
);end if

  (if (dictsearch c_dict el_name) (entdel (dictremove c_dict el_name)))

  (setq elem ( append (list
		(cons 0 "XRECORD")
		(cons 100 "AcDbXrecord")
	     ) elem)
	);end setq

  (setq elem (entmakex elem))
  (dictadd c_dict el_name elem)
);end defun


(defun TG_X-record_list (dict / )

  (if (setq dict (dictsearch (namedobjdict) dict))

  	(tg_get_DXF_pairs (list 3 350) dict)
	    

  );end if

);end defun

(defun tg_x-record_load (dict el_name / elem)

(if  (setq dict ( assoc -1 (dictsearch (namedobjdict) dict ))) 
	(progn
  	(setq elem (dictsearch (cdr dict) el_name ))
  	(tg_get_DXF_pairs (list
			    300
			    301
			    302
			    303
			    304
			    305
			    306
			    307
			    308
			    309
			    40
			    41
			    42
			    43
			    44
			    45
			    46
			    47
			    70
			    71
			    72
			    73
			    74
			    75
			    76
			    77
			    78
			    )



	  elem)
	    
	);end progn
  );end if



  );end defun


(defun tg_get_scale (/)
;;;
;;;���������� ������� �������
;;;
(cdr (assoc 41 (tg_x-record_load "TG_plan_parameters" "TG_plan_parameters")))
); end defun





(defun tg_refresh_x-record (dict m_enable / count message)

;;;��������� ��������� ������� ���� ��������-���������� � �������
;;;dict.
;;;� ������ ������������ - ������� ����� ���������
;;;���� ���������� m_enable - T ����� �������� ��������� �
;;;����������� ��������� �� ������ ������ ���������
;;;
;;;�������� ������ ��� �������� ��������� ������� �� ����������


(setq count 0 message "")
(mapcar (function (lambda (x / count upd_rec)

		(setq count 0)
		(mapcar (function (lambda (y / ent)
			(if (and (setq ent (handent (cdr y))) (entget ent)) (setq upd_rec (append upd_rec (list y))) (setq count (1+ count)))
				  ))

		         (tg_get_DXF_pairs (list 300 301 302 303 304 305) (tg_x-record_load dict x)))
		 (if (/= count 0)
		  (progn
		   (setq message (strcat message "������� " (itoa count) " �������� �� ������� " x " /n "))
		   (TG_X-record_save dict x upd_rec)
		  )
		    )))



 (mapcar 'cdr
	(vl-remove-if-not
	  (function (lambda (x) (= (car x) 3)))
	       (TG_X-record_list dict))
	))

  (if (and m_enable (/= message "")) (alert message))
);end defun


(defun tg_find_unknown_points ( / ss_filter onimport_blocks)
(tg_ActiveX_start)  
(setq ss_filter	(mapcar 'cdr (tg_x-record_load "TG_onimport_entries" "TG_onimport_blocks")))  

(setq onimport_blocks  (apply 'append

       			(mapcar (function (lambda (y)

				(mapcar 'cdr (tg_x-record_load "TG_Onimport" y))
					   ))

			(mapcar 'cdr
       			(vl-remove-if-not (function (lambda (x) (= (car x) 3)))
			  (TG_X-record_list "TG_Onimport"))))))


      (vl-remove-if (function (lambda (y)

				(member (cdr (assoc 5 (entget y))) onimport_blocks)
				    
				    ))

       (vl-remove-if-not (function (lambda (x)
			(and (= 'ENAME (type x)) (member (vla-get-EffectiveName (vlax-ename->vla-object x)) ss_filter))
      
			      ))
  (mapcar 'cadr (ssnamex (ssget "_X" (list (cons 0 "insert")))))))

       
);end defun


(defun tg_registrate_block (block / )

(if (not block)

 (progn

  (setq block (car (entsel)))
  (if (and (= "INSERT" (cdr (assoc 0 (entget block))))
	   (tg_get_following_data block)
	   )
	(setq block (vla-get-EffectiveName (vlax-ename->vla-object block)))
        (progn (princ "\n\n��������� ������ �� �������� ������, ������� ����� ���� ���������������")(exit))
    );_if

   );_progn
  );_if

(if (not (member block (mapcar 'cdr (tg_x-record_load "TG_onimport_entries" "TG_onimport_blocks"))) )

	(tg_x-record_save "TG_onimport_entries"
	  		  "TG_onimport_blocks"
	  		  (append
			    (tg_x-record_load "TG_onimport_entries" "TG_onimport_blocks")
			    (list(cons 300 block))
			  )
	)
)

);end defun



